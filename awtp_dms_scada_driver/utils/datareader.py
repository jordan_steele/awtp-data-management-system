# Citech Trend Data File Reader

# Has to be modified to read structure of XML file? For future scaling
# Only written for Type EightByteV600.. Need to support TwoByteV600

# Based off:
# http://proscada.ru/ctkbase.en/articles/q4312.html

# Command line args

import sys
from struct import *

totalargs = len(sys.argv)
cmdargs = sys.argv

if totalargs < 2:
    exit("Missing File Argument. \
            Example usage: >>> python histreader.py L3045_PV.000")

# First arg is filename
filename = cmdargs[1]
f = open(filename,'rb')

# For debugging
def bin2hex(binary):
    return map(hex,map(ord,binary))

# Reads the next num_bytes in an openfile
def nextbytes(openfile, num_bytes):
    return openfile.read(num_bytes)

class Version:
    TwoByteV600 = 5
    EightByteV600 = 6

# All information in trend file will appear as DATA_foobar

# Title
DATA_title = nextbytes(f, 112)

# SCALES
DATA_scales         = unpack('ffff', nextbytes(f, 16))
DATA_scales_RawZero = DATA_scales[0]       # Min raw value
DATA_scales_RawFull = DATA_scales[1]       # Max raw value
DATA_scales_EngZero = DATA_scales[2]       # Min eng (scaled) value
DATA_scales_EngFull = DATA_scales[3]       # Max eng (scaled) value

# HEADER
DATA_header_ID      = nextbytes(f, 8)                   # Usually CITECH
DATA_header_Type    = unpack('H', nextbytes(f, 2))[0]   # 0=FILE_TYPE_TREND
DATA_header_Version = unpack('H', nextbytes(f, 2))[0]   # 5=TwoByte, 6=EightByte

# Parse depending on Version
if (DATA_header_Version == Version.TwoByteV600):
    # Parse as TwoByteV600
    pass
elif (DATA_header_Version == Version.EightByteV600):
    print "EightByteV600 Detected"
    DATA_header_StartEvNo   = unpack('q',nextbytes(f, 8))[0]# StartEnvNo
    nextbytes(f, 12)                                        # Alignment
    DATA_header_LogName     = nextbytes(f,80)               # LogName
    DATA_header_Mode        = unpack('I',nextbytes(f,4))[0] # Mode
    DATA_header_Area        = unpack('H',nextbytes(f,2))[0] # Area
    DATA_header_Priv        = unpack('H',nextbytes(f,2))[0] # Priv
    DATA_header_FileType    = unpack('H',nextbytes(f,2))[0] # FileType
    DATA_header_SamplePeriod= unpack('I',nextbytes(f,4))[0] # SamplePeriod (ms)
    DATA_header_sEngUnits   = nextbytes(f, 8)[0]            # sEngUnits
    DATA_header_Format      = unpack('I',nextbytes(f,4))[0] # Format
    DATA_header_StartTime   = unpack('Q',nextbytes(f,8))[0] # StartTime
    DATA_header_EndTime     = unpack('Q',nextbytes(f,8))[0] # EndTime
    DATA_header_DataLength  = unpack('I',nextbytes(f,4))[0] # DataLength
    DATA_header_FilePointer = unpack('I',nextbytes(f,4))[0] # FilePointer
    DATA_header_EndEnvNo    = unpack('q',nextbytes(f,8))[0] # EndEnvNo
    nextbytes(f, 6)                                         # Alignment

    # For Float Event Sample
    # Will need to Read this way till end of file
    print unpack('dQ',nextbytes(f,16))                      # (Value, Time)
else:
    print "Unsupported file version: " + str(DATA_header_Version) + " detected."


# Turn into .csv, or whatever.
