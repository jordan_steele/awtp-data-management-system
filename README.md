## Development Team:
- Jordan Steele
- Jun Min Cheong
- Xibo Wang
- Xiangyu Zhou

### The AWTP Data Management System consists of 3 modules:
1. Backend REST API (served by Ruby on Rails)
2. Frontend AngularJS App (served by Node/ExpressJS)
3. SCADA Driver (bash scripts, etc.)

### Backend REST API :./awtp_dms_api/
- Contains the Ruby on Rails app that handles backend processing.
- Interfaces with other modules through a REST API

### Frontend AngularJS App : ./awtp_web_interface/
- Contains the Node app that the Angular is will be served on.

### SCADA Driver : ./awtp_dms_scada_driver/
- Contains stuff necessary to get files off SCADA
- Likely a bash script with automated VPN login
- Would also contain any scripts necessary to perform preprocessing on
  SCADA files (eg. convert to CSV)

### Clone the repo

Clone the repository using [git][git]:

```
git clone https://jordan_steele@bitbucket.org/jordan_steele/awtp-data-management-system.git
cd awtp-data-management-system
```

### Installation:
See individual project READMEs for installation and development instructions.

[git]: http://git-scm.com/