# Usage.. rake import_csv:write_foobar
# Will need to do date checking here? Probably...
# Use constantize for arbitrary table names...
# Right now, everything will be in seeds.rb.. till SCADA driver complete.

require 'csv'

# Temporary.. we'll stick this in a proper config file in a bit
csv_clone_path = "../awtp_dms_scada_driver/csv_clone/"

namespace :import_csv do
  task :write_trends => :environment do
    CSV.foreach(csv_clone_path + 'trend.csv', :encoding => 'iso-8859-1:utf-8', :headers => true) do |row|
      new_hash = {}
      row.to_hash.each_pair do |k,v|
       new_hash.merge!({k.downcase => v}) 
      end
      Trend.create!(new_hash)
    end
  end
  
  task :write_digalms => :environment do
    CSV.foreach(csv_clone_path + 'digalm.csv', :encoding => 'iso-8859-1:utf-8', :headers => true) do |row|
      new_hash = {}
      row.to_hash.each_pair do |k,v|
       new_hash.merge!({k.downcase => v}) 
      end
      Digalm.create!(new_hash)
    end
  end

  task :write_variables => :environment do
    CSV.foreach(csv_clone_path + 'variable.csv', :encoding => 'iso-8859-1:utf-8', :headers => true) do |row|
      new_hash = {}
      row.to_hash.each_pair do |k,v|
       new_hash.merge!({k.downcase => v}) 
      end
      Variable.create!(new_hash)
    end
  end
  
  # Does not add trend data yet.. We don't have them
  # This tasks creates migration files for missing trends
  task :add_missing_trends => :environment do
    missing_trends = ''
    # We attempt to generate models for all missing_trends
    Trend.all.each do |trend|
        begin
            # Tries to get trend as a model.. else fallback to rescue
            model_class_name = ("TREND_"+trend.name).underscore.camelize 
            trendmodel = model_class_name.constantize
            #system("rails generate model ThisTestModel name:string age:string")
            #puts 'TREND_'+trend.name.constantize
        rescue NameError
            missing_trends = missing_trends + trend.name + ", "
            # We assume only 8 byte model...
            if trend.stormethod == "Floating Point (8-byte samples)"
                #system("rails generate model TREND_"+trend.name+" "
                puts system("rails generate model TREND_"+trend.name+" value:float time:timestamp")
            else
                puts trend.name+" is of unsupported stormethod "+trend.stormethod+"!"
            end
        end
    end
    # Check if there are missing trends
    if missing_trends.length > 0
        puts "ActiveRecord models generated for trends: " + missing_trends[0..-3]
        puts "Work In Progress: Need to think of some way to put in data for trends other than seeding"
    else
        puts "All Trend models appear to already exist!"
    end
  end

  task :addnewtrend, [:name] => :environment do |t,args|
    puts "Added Trend "+args.name+"!"
    Trend.create!({ name: args.name })
  end

  task :test => :environment do
    Trend.all.each do |trend|
        begin
            # Tries to get trend as a model.. else fallback to rescue
            model_class_name = ("TREND_"+trend.name).underscore.camelize
            trendmodel = model_class_name.constantize
        end
    end
  end
end
