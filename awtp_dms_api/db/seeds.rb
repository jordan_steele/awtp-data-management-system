# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
def self.rand_range (min, max)
    rand * (max-min) + min
end

tags = Tag.create([
  { name: 'tag0001', value:10},
  { name: 'tag0002', value:20},
  { name: 'tag0003', value:22},
  { name: 'tag0004', value:45},
  { name: 'tag0005', value:63},
  { name: 'tag0006', value:113},
  { name: 'tag0007', value:1},
  { name: 'tag0008', value:14},
  { name: 'tag0009', value:12},
  { name: 'tag0010', value:12},
  { name: 'tag0011', value:40},
  { name: 'tag0012', value:54},
  { name: 'tag0013', value:112},
  ]);

users = User.create([
  { email: 'user1@example.com', password: 'secretsss', password_confirmation: 'secretsss', admin: true},
  { email: 'user2@example.com', password: 'secretsss', password_confirmation: 'secretsss'},
  { email: 'user3@example.com', password: 'secretsss', password_confirmation: 'secretsss'},
  { email: 'user4@example.com', password: 'secretsss', password_confirmation: 'secretsss'},
  { email: 'user5@example.com', password: 'secretsss', password_confirmation: 'secretsss'},
  { email: 'user6@example.com', password: 'secretsss', password_confirmation: 'secretsss'},
  { email: 'user7@example.com', password: 'secretsss', password_confirmation: 'secretsss'},
  { email: 'user8@example.com', password: 'secretsss', password_confirmation: 'secretsss'},
  { email: 'user9@example.com', password: 'secretsss', password_confirmation: 'secretsss'},
  ]);

equipments = Equipment.create([
  { location: 'AWTP Davis Station', model: 'GT2341', equipment_number: '1', 
    serial_number: 'SDFDS2342', make: 'Sony'},
  { location: 'AWTP Davis Station', model: 'LTO35243', equipment_number: '5', 
    serial_number: 'FDSF2345', make: 'Tony'},
  { location: 'AWTP Davis Station', model: 'Lazurus', equipment_number: '23', 
    serial_number: 'GDFGD5345', make: 'Fony'},
  { location: 'AWTP Davis Station', model: 'Slick', equipment_number: '42', 
    serial_number: 'KJNKJ3453', make: 'Dell'},
  { location: 'AWTP Davis Station', model: 'Slush', equipment_number: '74', 
    serial_number: 'BHIFD7435', make: 'Omnicorp'},
  { location: 'AWTP Davis Station', model: 'Meca', equipment_number: '83', 
    serial_number: 'DSFKN8345', make: 'Wayne Enterprises'},
  { location: 'AWTP Davis Station', model: 'Speedy', equipment_number: '2', 
    serial_number: 'KLBFD23426', make: 'Golith National'},
  { location: 'AWTP Davis Station', model: 'DF24224', equipment_number: '53', 
    serial_number: 'DSFN6747', make: 'Platinum Worldwide'},
  { location: 'AWTP Davis Station', model: 'Swag', equipment_number: '73', 
    serial_number: 'SNDFJL4534', make: 'YOLOTECH inc.'},
  { location: 'AWTP Davis Station', model: 'Galaxy', equipment_number: '82', 
    serial_number: 'JNKDSFL453', make: 'Asus'},
  { location: 'AWTP Davis Station', model: '64', equipment_number: '64', 
    serial_number: 'SDFK32422', make: 'Nintendo'},
  { location: 'AWTP Davis Station', model: 'MBP', equipment_number: '95', 
    serial_number: 'WPDDD4354', make: 'Apple'},
  ]);

analysts = ['Craig', 'Bob', 'Jack', 'James', 'Grace', 'Claire', 'Frank', 'Cesca',
  'Fred', 'Jess', 'Sarah']
comments = ['Pass', 'Fail']
change_of_buffers = [true, false]
check_or_calibrations = ['CK', 'CAL']

(1..500).each do |n|
  Colorimeter.create({equipment_id: equipments.sample.id, date: DateTime.now,
    analyst: analysts.sample, sol1: Random.rand(1000), sol2: Random.rand(1000),
    sol3: Random.rand(1000), comments: comments.sample})
end

(1..500).each do |n|
  PhCalibration.create(
    { equipment_id: equipments.sample.id, date: DateTime.now, analyst: analysts.sample, 
      change_of_buffers: change_of_buffers.sample, check_or_calibration: check_or_calibrations.sample, 
      buffer_calibration: 'pH ' + (1 + Random.rand(14)).to_s, slope: Random.rand(90), 
      comments: comments.sample })
end

require 'csv'

# Temporary.. we'll stick this in a proper config file in a bit
csv_clone_path = "../awtp_dms_scada_driver/csv_clone/"

# Add stuff for Variable
CSV.foreach(csv_clone_path + 'variable.csv', :encoding => 'iso-8859-1:utf-8', :headers => true) do |row|
    new_hash = {}
    row.to_hash.each_pair do |k,v|
        new_hash.merge!({k.downcase => v}) 
    end
    Variable.create!(new_hash)
end

puts "Variables seeded"

# Add stuff for Digalm
CSV.foreach(csv_clone_path + 'digalm.csv', :encoding => 'iso-8859-1:utf-8', :headers => true) do |row|
    new_hash = {}
    row.to_hash.each_pair do |k,v|
        new_hash.merge!({k.downcase => v}) 
    end
    Digalm.create!(new_hash)
end

puts "Dig Alarms seeded"

# Add stuff for Trend
CSV.foreach(csv_clone_path + 'trend.csv', :encoding => 'iso-8859-1:utf-8', :headers => true) do |row|
    new_hash = {}
    row.to_hash.each_pair do |k,v|
        new_hash.merge!({k.downcase => v}) 
    end
    Trend.create!(new_hash)
end

puts "Trends seeded"

# Now we look through trends and attempt to add Trend data.
total_trends = Trend.count
trends_completed = 0
missing_trends = ''
Trend.all.each do |trend|
    begin
        trendmodel = ("Trend" + trend.name.underscore.camelize).constantize
        # Now we grab all data from csv of this particular trend
        # ^^^ for now just putting in random data
        curr_value = 0.5
        (1..500).each do |n|
          curr_value += rand_range(-0.5, 0.5)
          trendmodel.create!(value: curr_value, time: DateTime.now+n)
        end
    rescue NameError
        missing_trends = missing_trends + trend.name + ", "
    end
    trends_completed += 1
    STDOUT.write "\rTrend models percent seeded #{trends_completed.to_f / total_trends * 100}"
end

# Check if there are missing trends
if missing_trends.length > 0
    puts "New Trends appear to have been added: " + missing_trends[0..-3]
    puts "Please run rake import_csv:add_missing_trends"
else
    puts "All Trends successfully added to database!"
end
