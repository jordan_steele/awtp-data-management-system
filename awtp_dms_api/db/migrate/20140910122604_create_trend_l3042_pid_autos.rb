class CreateTrendL3042PidAutos < ActiveRecord::Migration
  def change
    create_table :trend_l3042_pid_autos do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
