class CreateTrendMfCleanSeqSpBackWashRecipes < ActiveRecord::Migration
  def change
    create_table :trend_mf_clean_seq_sp_back_wash_recipes do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
