class CreateTrendL3280Statuses < ActiveRecord::Migration
  def change
    create_table :trend_l3280_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
