class CreateTrendL3030PidPvs < ActiveRecord::Migration
  def change
    create_table :trend_l3030_pid_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
