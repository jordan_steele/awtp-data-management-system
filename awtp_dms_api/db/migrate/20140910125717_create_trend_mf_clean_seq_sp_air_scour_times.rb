class CreateTrendMfCleanSeqSpAirScourTimes < ActiveRecord::Migration
  def change
    create_table :trend_mf_clean_seq_sp_air_scour_times do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
