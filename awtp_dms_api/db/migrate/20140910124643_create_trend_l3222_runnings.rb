class CreateTrendL3222Runnings < ActiveRecord::Migration
  def change
    create_table :trend_l3222_runnings do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
