class CreateTrendL3047Statuses < ActiveRecord::Migration
  def change
    create_table :trend_l3047_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
