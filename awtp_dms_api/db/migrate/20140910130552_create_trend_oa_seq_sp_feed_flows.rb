class CreateTrendOaSeqSpFeedFlows < ActiveRecord::Migration
  def change
    create_table :trend_oa_seq_sp_feed_flows do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
