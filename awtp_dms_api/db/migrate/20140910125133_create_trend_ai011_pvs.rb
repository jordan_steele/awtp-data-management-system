class CreateTrendAi011Pvs < ActiveRecord::Migration
  def change
    create_table :trend_ai011_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
