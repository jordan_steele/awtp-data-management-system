class CreateTrendL3121PidProgOpers < ActiveRecord::Migration
  def change
    create_table :trend_l3121_pid_prog_opers do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
