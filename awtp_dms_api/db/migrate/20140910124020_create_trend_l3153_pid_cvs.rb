class CreateTrendL3153PidCvs < ActiveRecord::Migration
  def change
    create_table :trend_l3153_pid_cvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
