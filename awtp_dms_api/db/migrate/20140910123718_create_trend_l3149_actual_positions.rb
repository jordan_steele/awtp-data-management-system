class CreateTrendL3149ActualPositions < ActiveRecord::Migration
  def change
    create_table :trend_l3149_actual_positions do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
