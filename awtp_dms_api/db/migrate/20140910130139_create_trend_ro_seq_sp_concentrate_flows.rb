class CreateTrendRoSeqSpConcentrateFlows < ActiveRecord::Migration
  def change
    create_table :trend_ro_seq_sp_concentrate_flows do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
