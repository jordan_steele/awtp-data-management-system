class CreateTrendL3153PidAutos < ActiveRecord::Migration
  def change
    create_table :trend_l3153_pid_autos do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
