class CreateTurbidities < ActiveRecord::Migration
  def change
    create_table :turbidities do |t|
      t.references :equipment, index: true
      t.datetime :Formazin_Calibration_Date
      t.string :Formazin_Comments
      t.datetime :Gelex_Check_Date
      t.string :Gelex_Comments
      t.string :Other_Comments

      t.timestamps
    end
  end
end
