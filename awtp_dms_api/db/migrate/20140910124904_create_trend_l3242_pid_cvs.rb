class CreateTrendL3242PidCvs < ActiveRecord::Migration
  def change
    create_table :trend_l3242_pid_cvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
