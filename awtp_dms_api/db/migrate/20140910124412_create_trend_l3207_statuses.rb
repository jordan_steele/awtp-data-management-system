class CreateTrendL3207Statuses < ActiveRecord::Migration
  def change
    create_table :trend_l3207_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
