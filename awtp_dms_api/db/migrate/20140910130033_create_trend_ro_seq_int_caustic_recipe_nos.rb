class CreateTrendRoSeqIntCausticRecipeNos < ActiveRecord::Migration
  def change
    create_table :trend_ro_seq_int_caustic_recipe_nos do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
