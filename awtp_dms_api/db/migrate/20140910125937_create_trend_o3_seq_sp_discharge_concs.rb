class CreateTrendO3SeqSpDischargeConcs < ActiveRecord::Migration
  def change
    create_table :trend_o3_seq_sp_discharge_concs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
