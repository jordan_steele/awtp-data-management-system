class CreateTrendL3175Pvs < ActiveRecord::Migration
  def change
    create_table :trend_l3175_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
