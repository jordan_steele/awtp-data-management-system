class CreateTrendL3241Statuses < ActiveRecord::Migration
  def change
    create_table :trend_l3241_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
