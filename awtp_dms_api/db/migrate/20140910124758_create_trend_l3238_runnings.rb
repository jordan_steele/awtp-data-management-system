class CreateTrendL3238Runnings < ActiveRecord::Migration
  def change
    create_table :trend_l3238_runnings do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
