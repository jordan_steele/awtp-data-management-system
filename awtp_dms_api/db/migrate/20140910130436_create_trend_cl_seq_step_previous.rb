class CreateTrendClSeqStepPrevious < ActiveRecord::Migration
  def change
    create_table :trend_cl_seq_step_previous do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
