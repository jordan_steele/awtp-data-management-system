class UpdateEquipmentColumns < ActiveRecord::Migration
  def up
    rename_column :equipment, :Location, :location
    rename_column :equipment, :Model, :model
    rename_column :equipment, :Equipment_Number, :equipment_number
    rename_column :equipment, :Serial_Number, :serial_number
    rename_column :equipment, :Make, :make
  end

  def down
    rename_column :equipment, :location, :Location
    rename_column :equipment, :model, :Model
    rename_column :equipment, :equipment_number, :Equipment_Number
    rename_column :equipment, :serial_number, :Serial_Number
    rename_column :equipment, :make, :Make
  end
end
