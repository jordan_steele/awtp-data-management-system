class CreateTrendL3113PidPvs < ActiveRecord::Migration
  def change
    create_table :trend_l3113_pid_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
