class CreateTrendMfSeqStepPrevious < ActiveRecord::Migration
  def change
    create_table :trend_mf_seq_step_previous do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
