class CreateTrendBacSeqSpFiltrateTurbidities < ActiveRecord::Migration
  def change
    create_table :trend_bac_seq_sp_filtrate_turbidities do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
