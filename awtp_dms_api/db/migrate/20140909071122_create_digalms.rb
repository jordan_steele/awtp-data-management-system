class CreateDigalms < ActiveRecord::Migration
  def change
      create_table "digalms" do |t|
        t.column "tag", :string, :limit => 79
        t.column "name", :string, :limit => 79
        t.column "desc", :string, :limit => 127
        t.column "var_a", :string, :limit => 254
        t.column "var_b", :string, :limit => 254
        t.column "category", :integer
        t.column "help", :string, :limit => 64
        t.column "priv", :string, :limit => 16
        t.column "area", :string, :limit => 16
        t.column "comment", :string, :limit => 48
        t.column "sequence", :string, :limit => 16
        t.column "delay", :string, :limit => 16
        t.column "custom1", :string, :limit => 64
        t.column "custom2", :string, :limit => 64
        t.column "custom3", :string, :limit => 64
        t.column "custom4", :string, :limit => 64
        t.column "custom5", :string, :limit => 64
        t.column "custom6", :string, :limit => 64
        t.column "custom7", :string, :limit => 64
        t.column "custom8", :string, :limit => 64
        t.column "cluster", :string, :limit => 16
        t.column "taggenlink", :string, :limit => 32
        t.column "paging", :string, :limit => 8
        t.column "paginggrp", :string, :limit => 80
      end
  end
end
