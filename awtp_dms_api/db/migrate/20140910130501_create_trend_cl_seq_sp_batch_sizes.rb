class CreateTrendClSeqSpBatchSizes < ActiveRecord::Migration
  def change
    create_table :trend_cl_seq_sp_batch_sizes do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
