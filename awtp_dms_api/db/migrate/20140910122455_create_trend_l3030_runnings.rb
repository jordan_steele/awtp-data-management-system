class CreateTrendL3030Runnings < ActiveRecord::Migration
  def change
    create_table :trend_l3030_runnings do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
