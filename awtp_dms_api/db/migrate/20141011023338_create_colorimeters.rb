class CreateColorimeters < ActiveRecord::Migration
  def change
    create_table :colorimeters do |t|
      t.references :equipment, index: true
      t.datetime :Date
      t.string :Analyst
      t.string :Type
      t.string :Sol1
      t.string :Sol2
      t.string :Sol3
      t.string :Comments

      t.timestamps
    end
  end
end
