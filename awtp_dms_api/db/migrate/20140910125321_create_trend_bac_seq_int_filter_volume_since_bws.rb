class CreateTrendBacSeqIntFilterVolumeSinceBws < ActiveRecord::Migration
  def change
    create_table :trend_bac_seq_int_filter_volume_since_bws do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
