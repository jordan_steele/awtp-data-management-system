class CreateTrendL3242ActualSetPoints < ActiveRecord::Migration
  def change
    create_table :trend_l3242_actual_set_points do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
