class CreateTrendBacSeqSpHighRateTimes < ActiveRecord::Migration
  def change
    create_table :trend_bac_seq_sp_high_rate_times do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
