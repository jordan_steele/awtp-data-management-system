class CreateTrendL3030ActualSetPoints < ActiveRecord::Migration
  def change
    create_table :trend_l3030_actual_set_points do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
