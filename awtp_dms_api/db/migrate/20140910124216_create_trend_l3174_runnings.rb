class CreateTrendL3174Runnings < ActiveRecord::Migration
  def change
    create_table :trend_l3174_runnings do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
