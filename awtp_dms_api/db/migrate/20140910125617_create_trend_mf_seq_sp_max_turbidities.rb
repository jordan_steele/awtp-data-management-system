class CreateTrendMfSeqSpMaxTurbidities < ActiveRecord::Migration
  def change
    create_table :trend_mf_seq_sp_max_turbidities do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
