class CreateTrendMfCleanSeqIntNo1FilterTotalBws < ActiveRecord::Migration
  def change
    create_table :trend_mf_clean_seq_int_no1_filter_total_bws do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
