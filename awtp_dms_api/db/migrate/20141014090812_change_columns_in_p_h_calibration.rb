class ChangeColumnsInPHCalibration < ActiveRecord::Migration
  def up
    rename_column :p_h_calibrations, :Date, :date
    rename_column :p_h_calibrations, :Analyst, :analyst
    rename_column :p_h_calibrations, :Change_Of_Buffers, :change_of_buffers
    rename_column :p_h_calibrations, :Check_or_Calibration, :check_or_calibration
    rename_column :p_h_calibrations, :BufferCalibration4, :buffer_calibration_4
    rename_column :p_h_calibrations, :BufferCalibration7, :buffer_calibration_7
    rename_column :p_h_calibrations, :BufferCalibration10, :buffer_calibration_10
    rename_column :p_h_calibrations, :Slope, :slope
    rename_column :p_h_calibrations, :Comments, :comments
  end

  def down
    rename_column :p_h_calibrations, :date, :Date
    rename_column :p_h_calibrations, :analyst, :Analyst
    rename_column :p_h_calibrations, :change_of_buffers, :Change_Of_Buffers
    rename_column :p_h_calibrations, :check_or_calibration, :Check_or_Calibration
    rename_column :p_h_calibrations, :buffer_calibration_4, :BufferCalibration4
    rename_column :p_h_calibrations, :buffer_calibration_7, :BufferCalibration7
    rename_column :p_h_calibrations, :buffer_calibration_10, :BufferCalibration10
    rename_column :p_h_calibrations, :slope, :Slope
    rename_column :p_h_calibrations, :comments, :Comments
  end
end
