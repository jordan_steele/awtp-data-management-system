class CreateTrendRoSeqSpAcidWashTimes < ActiveRecord::Migration
  def change
    create_table :trend_ro_seq_sp_acid_wash_times do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
