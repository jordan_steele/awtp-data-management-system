class CreateEquipment < ActiveRecord::Migration
  def change
    create_table :equipment do |t|
      t.string :Location
      t.string :Model
      t.string :Equipment_Number
      t.string :Serial_Number
      t.string :Make

      t.timestamps
    end
  end
end
