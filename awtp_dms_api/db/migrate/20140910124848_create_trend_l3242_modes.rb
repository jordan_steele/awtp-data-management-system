class CreateTrendL3242Modes < ActiveRecord::Migration
  def change
    create_table :trend_l3242_modes do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
