class CreateTrendL3042Modes < ActiveRecord::Migration
  def change
    create_table :trend_l3042_modes do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
