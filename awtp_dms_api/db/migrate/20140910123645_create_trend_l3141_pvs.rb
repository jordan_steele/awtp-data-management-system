class CreateTrendL3141Pvs < ActiveRecord::Migration
  def change
    create_table :trend_l3141_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
