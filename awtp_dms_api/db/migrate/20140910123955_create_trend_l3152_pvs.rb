class CreateTrendL3152Pvs < ActiveRecord::Migration
  def change
    create_table :trend_l3152_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
