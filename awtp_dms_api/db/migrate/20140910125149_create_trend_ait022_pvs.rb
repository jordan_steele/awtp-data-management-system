class CreateTrendAit022Pvs < ActiveRecord::Migration
  def change
    create_table :trend_ait022_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
