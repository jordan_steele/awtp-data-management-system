class CreateTrendAi013Pvs < ActiveRecord::Migration
  def change
    create_table :trend_ai013_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
