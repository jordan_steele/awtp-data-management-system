class CreateTrendRoSeqIntPdtDecayRates < ActiveRecord::Migration
  def change
    create_table :trend_ro_seq_int_pdt_decay_rates do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
