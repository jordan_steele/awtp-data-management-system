class CreateTrendL3146Pvs < ActiveRecord::Migration
  def change
    create_table :trend_l3146_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
