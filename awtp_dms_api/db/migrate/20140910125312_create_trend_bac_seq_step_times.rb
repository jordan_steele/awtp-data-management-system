class CreateTrendBacSeqStepTimes < ActiveRecord::Migration
  def change
    create_table :trend_bac_seq_step_times do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
