class CreateTrendL3149LowerPositionLimits < ActiveRecord::Migration
  def change
    create_table :trend_l3149_lower_position_limits do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
