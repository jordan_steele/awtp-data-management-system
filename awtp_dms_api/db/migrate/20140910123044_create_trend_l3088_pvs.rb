class CreateTrendL3088Pvs < ActiveRecord::Migration
  def change
    create_table :trend_l3088_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
