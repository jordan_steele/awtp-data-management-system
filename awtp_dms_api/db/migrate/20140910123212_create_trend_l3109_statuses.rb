class CreateTrendL3109Statuses < ActiveRecord::Migration
  def change
    create_table :trend_l3109_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
