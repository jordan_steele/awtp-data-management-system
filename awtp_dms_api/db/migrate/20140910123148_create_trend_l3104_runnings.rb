class CreateTrendL3104Runnings < ActiveRecord::Migration
  def change
    create_table :trend_l3104_runnings do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
