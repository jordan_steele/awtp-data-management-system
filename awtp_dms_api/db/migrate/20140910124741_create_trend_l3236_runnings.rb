class CreateTrendL3236Runnings < ActiveRecord::Migration
  def change
    create_table :trend_l3236_runnings do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
