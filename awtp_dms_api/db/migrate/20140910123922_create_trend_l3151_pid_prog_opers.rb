class CreateTrendL3151PidProgOpers < ActiveRecord::Migration
  def change
    create_table :trend_l3151_pid_prog_opers do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
