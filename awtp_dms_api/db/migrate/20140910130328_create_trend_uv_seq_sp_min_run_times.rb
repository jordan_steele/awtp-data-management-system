class CreateTrendUvSeqSpMinRunTimes < ActiveRecord::Migration
  def change
    create_table :trend_uv_seq_sp_min_run_times do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
