class CreateTrendRoSeqSpMixTankOpLevels < ActiveRecord::Migration
  def change
    create_table :trend_ro_seq_sp_mix_tank_op_levels do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
