class CreateTrendL3221Pvs < ActiveRecord::Migration
  def change
    create_table :trend_l3221_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
