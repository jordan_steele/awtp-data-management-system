class CreateTrendL3279Runnings < ActiveRecord::Migration
  def change
    create_table :trend_l3279_runnings do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
