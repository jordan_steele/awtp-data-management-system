class CreateTrendMfSeqStepNumbers < ActiveRecord::Migration
  def change
    create_table :trend_mf_seq_step_numbers do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
