class CreateTrendL3113PidProgOpers < ActiveRecord::Migration
  def change
    create_table :trend_l3113_pid_prog_opers do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
