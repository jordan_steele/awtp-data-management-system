class CreateTrendL3151UpperPositionLimits < ActiveRecord::Migration
  def change
    create_table :trend_l3151_upper_position_limits do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
