class CreateTrendBacSeqSpMaxVolumeBeforeBws < ActiveRecord::Migration
  def change
    create_table :trend_bac_seq_sp_max_volume_before_bws do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
