class CreateTrendL3030PidSps < ActiveRecord::Migration
  def change
    create_table :trend_l3030_pid_sps do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
