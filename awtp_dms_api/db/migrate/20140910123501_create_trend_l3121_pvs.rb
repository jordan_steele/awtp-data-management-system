class CreateTrendL3121Pvs < ActiveRecord::Migration
  def change
    create_table :trend_l3121_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
