class CreateTrendO3SeqIntProcessedVolumes < ActiveRecord::Migration
  def change
    create_table :trend_o3_seq_int_processed_volumes do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
