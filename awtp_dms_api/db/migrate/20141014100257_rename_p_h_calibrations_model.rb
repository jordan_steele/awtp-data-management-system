class RenamePHCalibrationsModel < ActiveRecord::Migration
  def self.up
    rename_table :p_h_calibrations, :ph_calibrations
  end

  def self.down
    rename_table :ph_calibrations, :p_h_calibrations
  end
end
