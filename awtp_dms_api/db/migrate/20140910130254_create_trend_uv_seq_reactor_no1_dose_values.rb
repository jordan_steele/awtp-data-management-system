class CreateTrendUvSeqReactorNo1DoseValues < ActiveRecord::Migration
  def change
    create_table :trend_uv_seq_reactor_no1_dose_values do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
