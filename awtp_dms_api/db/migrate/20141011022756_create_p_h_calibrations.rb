class CreatePHCalibrations < ActiveRecord::Migration
  def change
    create_table :p_h_calibrations do |t|
      t.references :equipment, index: true
      t.datetime :Date
      t.string :Analyst
      t.boolean :Change_Of_Buffers
      t.string :Check_or_Calibration
      t.string :BufferCalibration4
      t.string :BufferCalibration7
      t.string :BufferCalibration10
      t.string :Slope
      t.string :Comments

      t.timestamps
    end
  end
end
