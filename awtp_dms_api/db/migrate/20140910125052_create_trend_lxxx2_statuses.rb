class CreateTrendLxxx2Statuses < ActiveRecord::Migration
  def change
    create_table :trend_lxxx2_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
