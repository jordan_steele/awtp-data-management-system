class CreateTurbidityRecords < ActiveRecord::Migration
  def change
    create_table :turbidity_records do |t|
      t.references :turbidity, index: true
      t.string :Type
      t.integer :Calibration_Check
      t.float :Range_Reading
      t.boolean :Tick

      t.timestamps
    end
  end
end
