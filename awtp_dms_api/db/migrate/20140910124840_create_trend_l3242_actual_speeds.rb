class CreateTrendL3242ActualSpeeds < ActiveRecord::Migration
  def change
    create_table :trend_l3242_actual_speeds do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
