class CreateTrendL3224Statuses < ActiveRecord::Migration
  def change
    create_table :trend_l3224_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
