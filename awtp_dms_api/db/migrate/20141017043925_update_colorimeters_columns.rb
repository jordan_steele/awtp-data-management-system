class UpdateColorimetersColumns < ActiveRecord::Migration
  def up
    rename_column :colorimeters, :Date, :date
    rename_column :colorimeters, :Analyst, :analyst
    remove_column :colorimeters, :Type
    rename_column :colorimeters, :Sol1, :sol1
    rename_column :colorimeters, :Sol2, :sol2
    rename_column :colorimeters, :Sol3, :sol3
    rename_column :colorimeters, :Comments, :comments
  end

  def down
    rename_column :colorimeters, :date, :Date
    rename_column :colorimeters, :analyst, :Analyst
    add_column :colorimeters, :type, :string
    rename_column :colorimeters, :sol1, :Sol1
    rename_column :colorimeters, :sol2, :Sol2
    rename_column :colorimeters, :sol3, :Sol3
    rename_column :colorimeters, :comments, :Comments
  end
end
