class CreateTrendL3079Pvs < ActiveRecord::Migration
  def change
    create_table :trend_l3079_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
