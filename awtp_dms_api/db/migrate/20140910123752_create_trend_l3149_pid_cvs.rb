class CreateTrendL3149PidCvs < ActiveRecord::Migration
  def change
    create_table :trend_l3149_pid_cvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
