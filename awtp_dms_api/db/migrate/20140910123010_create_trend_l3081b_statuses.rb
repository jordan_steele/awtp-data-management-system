class CreateTrendL3081bStatuses < ActiveRecord::Migration
  def change
    create_table :trend_l3081b_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
