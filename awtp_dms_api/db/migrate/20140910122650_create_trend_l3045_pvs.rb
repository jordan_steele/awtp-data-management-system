class CreateTrendL3045Pvs < ActiveRecord::Migration
  def change
    create_table :trend_l3045_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
