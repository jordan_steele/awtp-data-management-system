class CreateTrendCipSeqStepResets < ActiveRecord::Migration
  def change
    create_table :trend_cip_seq_step_resets do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
