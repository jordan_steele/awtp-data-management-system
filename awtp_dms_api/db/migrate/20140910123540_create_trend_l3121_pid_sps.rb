class CreateTrendL3121PidSps < ActiveRecord::Migration
  def change
    create_table :trend_l3121_pid_sps do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
