class CreateTrendUvSeqStepNumbers < ActiveRecord::Migration
  def change
    create_table :trend_uv_seq_step_numbers do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
