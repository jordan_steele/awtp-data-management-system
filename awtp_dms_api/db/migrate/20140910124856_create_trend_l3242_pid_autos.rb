class CreateTrendL3242PidAutos < ActiveRecord::Migration
  def change
    create_table :trend_l3242_pid_autos do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
