class CreateTrendL3124Pvs < ActiveRecord::Migration
  def change
    create_table :trend_l3124_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
