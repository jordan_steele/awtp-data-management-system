class CreateTrendCipSeqStepNumbers < ActiveRecord::Migration
  def change
    create_table :trend_cip_seq_step_numbers do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
