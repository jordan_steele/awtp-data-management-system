class CreateTrendMfCleanSeqSpNoBwBeforeCebs < ActiveRecord::Migration
  def change
    create_table :trend_mf_clean_seq_sp_no_bw_before_cebs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
