class CreateTrendMfSeqSpPdtDecayTimes < ActiveRecord::Migration
  def change
    create_table :trend_mf_seq_sp_pdt_decay_times do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
