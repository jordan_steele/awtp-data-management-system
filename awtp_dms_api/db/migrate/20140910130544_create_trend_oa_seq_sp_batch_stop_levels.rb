class CreateTrendOaSeqSpBatchStopLevels < ActiveRecord::Migration
  def change
    create_table :trend_oa_seq_sp_batch_stop_levels do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
