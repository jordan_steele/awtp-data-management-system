class CreateTrendL3052Statuses < ActiveRecord::Migration
  def change
    create_table :trend_l3052_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
