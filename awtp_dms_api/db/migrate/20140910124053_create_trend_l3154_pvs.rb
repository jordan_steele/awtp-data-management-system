class CreateTrendL3154Pvs < ActiveRecord::Migration
  def change
    create_table :trend_l3154_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
