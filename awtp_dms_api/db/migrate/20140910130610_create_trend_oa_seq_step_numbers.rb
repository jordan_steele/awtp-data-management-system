class CreateTrendOaSeqStepNumbers < ActiveRecord::Migration
  def change
    create_table :trend_oa_seq_step_numbers do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
