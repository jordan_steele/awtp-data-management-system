class CreateTrendL3065Statuses < ActiveRecord::Migration
  def change
    create_table :trend_l3065_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
