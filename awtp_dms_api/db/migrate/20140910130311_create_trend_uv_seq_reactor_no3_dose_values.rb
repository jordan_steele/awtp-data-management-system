class CreateTrendUvSeqReactorNo3DoseValues < ActiveRecord::Migration
  def change
    create_table :trend_uv_seq_reactor_no3_dose_values do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
