class CreateTrendUvSeqReactorNo2DoseValues < ActiveRecord::Migration
  def change
    create_table :trend_uv_seq_reactor_no2_dose_values do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
