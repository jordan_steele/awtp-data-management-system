class CreateTrendMfSeqIntNo2FilterPdtDecayRates < ActiveRecord::Migration
  def change
    create_table :trend_mf_seq_int_no2_filter_pdt_decay_rates do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
