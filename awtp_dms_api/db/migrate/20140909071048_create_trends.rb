class CreateTrends < ActiveRecord::Migration
  def change
      create_table "trends" do |t|
        t.column "name", :string, :limit => 79
        t.column "expr", :string, :limit => 254
        t.column "trig", :string, :limit => 254
        t.column "sampleper", :string, :limit => 16
        t.column "priv", :string, :limit => 16
        t.column "area", :string, :limit => 16
        t.column "eng_units", :string, :limit => 8
        t.column "format", :string, :limit => 11
        t.column "filename", :string, :limit => 253
        t.column "files", :string, :limit => 4
        t.column "time", :string, :limit => 32
        t.column "period", :string, :limit => 32
        t.column "comment", :string, :limit => 48
        t.column "trendtype", :string, :limit => 32
        t.column "spcflag", :string, :limit => 4
        t.column "lsl", :string, :limit => 16
        t.column "usl", :string, :limit => 16
        t.column "subgrpsize", :string, :limit => 8
        t.column "xdoublebar", :string, :limit => 16
        t.column "range", :string, :limit => 16
        t.column "sdeviation", :string, :limit => 16
        t.column "stormethod", :string, :limit => 64
        t.column "cluster", :string, :limit => 16
        t.column "taggenlink", :string, :limit => 32
      end
  end
end
