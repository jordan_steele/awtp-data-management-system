class CreateTrendL3269Pvs < ActiveRecord::Migration
  def change
    create_table :trend_l3269_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
