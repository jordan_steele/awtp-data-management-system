class CreateTrendL3153PidPvs < ActiveRecord::Migration
  def change
    create_table :trend_l3153_pid_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
