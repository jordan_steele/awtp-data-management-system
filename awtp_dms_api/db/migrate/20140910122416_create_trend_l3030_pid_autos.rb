class CreateTrendL3030PidAutos < ActiveRecord::Migration
  def change
    create_table :trend_l3030_pid_autos do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
