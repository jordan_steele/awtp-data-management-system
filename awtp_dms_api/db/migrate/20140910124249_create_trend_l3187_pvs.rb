class CreateTrendL3187Pvs < ActiveRecord::Migration
  def change
    create_table :trend_l3187_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
