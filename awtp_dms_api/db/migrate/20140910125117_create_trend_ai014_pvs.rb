class CreateTrendAi014Pvs < ActiveRecord::Migration
  def change
    create_table :trend_ai014_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
