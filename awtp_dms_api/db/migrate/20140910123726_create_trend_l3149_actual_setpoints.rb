class CreateTrendL3149ActualSetpoints < ActiveRecord::Migration
  def change
    create_table :trend_l3149_actual_setpoints do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
