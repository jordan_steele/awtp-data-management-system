class CreateTrendL3228Statuses < ActiveRecord::Migration
  def change
    create_table :trend_l3228_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
