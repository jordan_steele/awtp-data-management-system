class CreateTrendMfSeqIntNo1FilterVolumes < ActiveRecord::Migration
  def change
    create_table :trend_mf_seq_int_no1_filter_volumes do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
