class CreateTrendL3060Pvs < ActiveRecord::Migration
  def change
    create_table :trend_l3060_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
