class CreateTrendUvSeqSpMinUvIntensities < ActiveRecord::Migration
  def change
    create_table :trend_uv_seq_sp_min_uv_intensities do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
