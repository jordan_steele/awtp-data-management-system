class CreateTrendCipSeqReqEnables < ActiveRecord::Migration
  def change
    create_table :trend_cip_seq_req_enables do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
