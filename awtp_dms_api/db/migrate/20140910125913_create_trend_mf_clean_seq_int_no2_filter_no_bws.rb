class CreateTrendMfCleanSeqIntNo2FilterNoBws < ActiveRecord::Migration
  def change
    create_table :trend_mf_clean_seq_int_no2_filter_no_bws do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
