class CreateTrendL3070Statuses < ActiveRecord::Migration
  def change
    create_table :trend_l3070_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
