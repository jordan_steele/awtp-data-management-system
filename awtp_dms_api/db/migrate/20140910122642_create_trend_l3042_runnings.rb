class CreateTrendL3042Runnings < ActiveRecord::Migration
  def change
    create_table :trend_l3042_runnings do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
