class CreateTrendL3151ActualPositions < ActiveRecord::Migration
  def change
    create_table :trend_l3151_actual_positions do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
