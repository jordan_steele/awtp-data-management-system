class CreateTrendUvSeqReqEnables < ActiveRecord::Migration
  def change
    create_table :trend_uv_seq_req_enables do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
