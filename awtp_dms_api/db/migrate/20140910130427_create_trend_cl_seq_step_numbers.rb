class CreateTrendClSeqStepNumbers < ActiveRecord::Migration
  def change
    create_table :trend_cl_seq_step_numbers do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
