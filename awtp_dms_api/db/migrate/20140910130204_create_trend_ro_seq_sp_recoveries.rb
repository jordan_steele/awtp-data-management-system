class CreateTrendRoSeqSpRecoveries < ActiveRecord::Migration
  def change
    create_table :trend_ro_seq_sp_recoveries do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
