class ChangeColumnsInPHCalibration2 < ActiveRecord::Migration
  def up
    rename_column :p_h_calibrations, :buffer_calibration_4, :buffer_calibration
    remove_column :p_h_calibrations, :buffer_calibration_7
    remove_column :p_h_calibrations, :buffer_calibration_10
  end

  def down
    rename_column :p_h_calibrations, :buffer_calibration, :buffer_calibration_4
    add_column :p_h_calibrations, :buffer_calibration_7, :string
    add_column :p_h_calibrations, :buffer_calibration_10, :string
  end
end
