class CreateTrendL3081aStatuses < ActiveRecord::Migration
  def change
    create_table :trend_l3081a_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
