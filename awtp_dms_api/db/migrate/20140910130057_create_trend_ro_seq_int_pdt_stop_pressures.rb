class CreateTrendRoSeqIntPdtStopPressures < ActiveRecord::Migration
  def change
    create_table :trend_ro_seq_int_pdt_stop_pressures do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
