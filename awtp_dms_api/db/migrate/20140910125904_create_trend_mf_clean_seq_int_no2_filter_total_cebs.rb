class CreateTrendMfCleanSeqIntNo2FilterTotalCebs < ActiveRecord::Migration
  def change
    create_table :trend_mf_clean_seq_int_no2_filter_total_cebs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
