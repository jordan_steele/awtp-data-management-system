class CreateTrendL3042PidCvs < ActiveRecord::Migration
  def change
    create_table :trend_l3042_pid_cvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
