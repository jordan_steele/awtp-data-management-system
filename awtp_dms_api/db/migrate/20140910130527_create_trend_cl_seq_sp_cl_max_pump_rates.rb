class CreateTrendClSeqSpClMaxPumpRates < ActiveRecord::Migration
  def change
    create_table :trend_cl_seq_sp_cl_max_pump_rates do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
