class CreateTrendL3151ActualSetpoints < ActiveRecord::Migration
  def change
    create_table :trend_l3151_actual_setpoints do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
