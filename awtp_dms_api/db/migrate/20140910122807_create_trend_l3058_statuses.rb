class CreateTrendL3058Statuses < ActiveRecord::Migration
  def change
    create_table :trend_l3058_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
