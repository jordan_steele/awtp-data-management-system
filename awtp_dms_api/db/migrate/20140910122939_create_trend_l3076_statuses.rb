class CreateTrendL3076Statuses < ActiveRecord::Migration
  def change
    create_table :trend_l3076_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
