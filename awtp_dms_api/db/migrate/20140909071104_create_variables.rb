class CreateVariables < ActiveRecord::Migration
  def change
      create_table "variables" do |t|
        t.column "name", :string, :limit => 79
        t.column "variabletype", :string, :limit => 16
        t.column "unit", :string, :limit => 31
        t.column "addr", :string, :limit => 254
        t.column "raw_zero", :string, :limit => 11
        t.column "raw_full", :integer
        t.column "eng_zero", :string, :limit => 11
        t.column "eng_full", :integer
        t.column "eng_units", :string, :limit => 8
        t.column "format", :string, :limit => 11
        t.column "comment", :string, :limit => 48
        t.column "editcode", :string, :limit => 8
        t.column "linked", :string, :limit => 1
        t.column "oid", :string, :limit => 10
        t.column "ref1", :string, :limit => 11
        t.column "ref2", :string, :limit => 11
        t.column "deadband", :string, :limit => 11
        t.column "custom", :string, :limit => 128
        t.column "taggenlink", :string, :limit => 32
        t.column "cluster", :string, :limit => 16
      end
  end
end
