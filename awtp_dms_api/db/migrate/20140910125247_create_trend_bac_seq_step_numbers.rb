class CreateTrendBacSeqStepNumbers < ActiveRecord::Migration
  def change
    create_table :trend_bac_seq_step_numbers do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
