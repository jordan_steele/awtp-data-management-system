class CreateTrendLxxx3Pvs < ActiveRecord::Migration
  def change
    create_table :trend_lxxx3_pvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
