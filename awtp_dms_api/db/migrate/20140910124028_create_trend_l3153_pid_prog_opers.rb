class CreateTrendL3153PidProgOpers < ActiveRecord::Migration
  def change
    create_table :trend_l3153_pid_prog_opers do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
