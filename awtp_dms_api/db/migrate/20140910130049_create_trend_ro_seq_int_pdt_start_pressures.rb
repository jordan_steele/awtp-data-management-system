class CreateTrendRoSeqIntPdtStartPressures < ActiveRecord::Migration
  def change
    create_table :trend_ro_seq_int_pdt_start_pressures do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
