class CreateTrendOaSeqStepTimes < ActiveRecord::Migration
  def change
    create_table :trend_oa_seq_step_times do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
