class CreateTrendL3121PidCvs < ActiveRecord::Migration
  def change
    create_table :trend_l3121_pid_cvs do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
