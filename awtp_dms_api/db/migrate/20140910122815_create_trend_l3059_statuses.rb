class CreateTrendL3059Statuses < ActiveRecord::Migration
  def change
    create_table :trend_l3059_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
