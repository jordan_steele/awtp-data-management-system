class CreateTrendL3113Runnings < ActiveRecord::Migration
  def change
    create_table :trend_l3113_runnings do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
