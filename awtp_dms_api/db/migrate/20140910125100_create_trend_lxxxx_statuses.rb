class CreateTrendLxxxxStatuses < ActiveRecord::Migration
  def change
    create_table :trend_lxxxx_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
