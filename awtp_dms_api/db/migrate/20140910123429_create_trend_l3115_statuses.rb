class CreateTrendL3115Statuses < ActiveRecord::Migration
  def change
    create_table :trend_l3115_statuses do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
