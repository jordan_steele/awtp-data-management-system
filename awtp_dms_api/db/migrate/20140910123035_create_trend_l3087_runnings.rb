class CreateTrendL3087Runnings < ActiveRecord::Migration
  def change
    create_table :trend_l3087_runnings do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
