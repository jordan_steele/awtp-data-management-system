class CreateTrendO3SeqSpOzoneConsumptions < ActiveRecord::Migration
  def change
    create_table :trend_o3_seq_sp_ozone_consumptions do |t|
      t.float :value
      t.timestamp :time

      t.timestamps
    end
  end
end
