# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141017043925) do

  create_table "colorimeters", force: true do |t|
    t.integer  "equipment_id"
    t.datetime "date"
    t.string   "analyst"
    t.string   "sol1"
    t.string   "sol2"
    t.string   "sol3"
    t.string   "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "colorimeters", ["equipment_id"], name: "index_colorimeters_on_equipment_id"

  create_table "digalms", force: true do |t|
    t.string  "tag",        limit: 79
    t.string  "name",       limit: 79
    t.string  "desc",       limit: 127
    t.string  "var_a",      limit: 254
    t.string  "var_b",      limit: 254
    t.integer "category"
    t.string  "help",       limit: 64
    t.string  "priv",       limit: 16
    t.string  "area",       limit: 16
    t.string  "comment",    limit: 48
    t.string  "sequence",   limit: 16
    t.string  "delay",      limit: 16
    t.string  "custom1",    limit: 64
    t.string  "custom2",    limit: 64
    t.string  "custom3",    limit: 64
    t.string  "custom4",    limit: 64
    t.string  "custom5",    limit: 64
    t.string  "custom6",    limit: 64
    t.string  "custom7",    limit: 64
    t.string  "custom8",    limit: 64
    t.string  "cluster",    limit: 16
    t.string  "taggenlink", limit: 32
    t.string  "paging",     limit: 8
    t.string  "paginggrp",  limit: 80
  end

  create_table "equipment", force: true do |t|
    t.string   "location"
    t.string   "model"
    t.string   "equipment_number"
    t.string   "serial_number"
    t.string   "make"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ph_calibrations", force: true do |t|
    t.integer  "equipment_id"
    t.datetime "date"
    t.string   "analyst"
    t.boolean  "change_of_buffers"
    t.string   "check_or_calibration"
    t.string   "buffer_calibration"
    t.string   "slope"
    t.string   "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ph_calibrations", ["equipment_id"], name: "index_ph_calibrations_on_equipment_id"

  create_table "tags", force: true do |t|
    t.string   "name"
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ai011_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ai013_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ai014_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ai015_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ait021_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ait022_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_bac_seq_int_filter_volume_since_bws", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_bac_seq_req_enables", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_bac_seq_sp_air_scour_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_bac_seq_sp_filtrate_turbidities", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_bac_seq_sp_high_rate_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_bac_seq_sp_max_headlosses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_bac_seq_sp_max_turbidities", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_bac_seq_sp_max_volume_before_bws", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_bac_seq_step_numbers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_bac_seq_step_previous", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_bac_seq_step_resets", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_bac_seq_step_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_cip_seq_req_enables", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_cip_seq_step_numbers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_cip_seq_step_previous", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_cip_seq_step_resets", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_cip_seq_step_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_cl_seq_req_enables", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_cl_seq_sp_batch_sizes", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_cl_seq_sp_cl_concentrations", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_cl_seq_sp_cl_max_pump_rates", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_cl_seq_sp_hypo_concentrations", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_cl_seq_step_numbers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_cl_seq_step_previous", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_cl_seq_step_resets", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_cl_seq_step_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3030_actual_set_points", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3030_actual_speeds", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3030_modes", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3030_pid_autos", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3030_pid_cvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3030_pid_prog_opers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3030_pid_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3030_pid_sps", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3030_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3031_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3032_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3033_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3038_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3041_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3042_actual_set_points", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3042_actual_speeds", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3042_modes", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3042_pid_autos", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3042_pid_cvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3042_pid_prog_opers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3042_pid_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3042_pid_sps", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3042_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3045_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3047_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3048_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3050_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3051_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3052_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3053_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3054_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3055_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3057_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3058_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3059_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3060_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3062_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3063_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3064_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3065_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3066_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3067_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3069_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3070_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3074_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3076_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3077_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3079_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3081a_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3081b_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3082_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3083_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3087_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3088_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3091_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3092_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3093_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3095_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3097_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3098_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3101_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3104_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3105_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3108_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3109_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3110_pid_autos", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3110_pid_cvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3110_pid_prog_opers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3110_pid_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3110_pid_sps", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3110_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3111_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3113_actual_set_points", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3113_actual_speeds", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3113_modes", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3113_pid_autos", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3113_pid_cvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3113_pid_prog_opers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3113_pid_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3113_pid_sps", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3113_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3115_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3117_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3118_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3119_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3121_pid_autos", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3121_pid_cvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3121_pid_prog_opers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3121_pid_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3121_pid_sps", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3121_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3124_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3126_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3129_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3131_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3134_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3136_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3139_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3141_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3144_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3146_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3148_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3149_actual_positions", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3149_actual_setpoints", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3149_lower_position_limits", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3149_pid_autos", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3149_pid_cvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3149_pid_prog_opers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3149_pid_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3149_pid_sps", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3149_upper_position_limits", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3150_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3151_actual_positions", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3151_actual_setpoints", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3151_lower_position_limits", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3151_pid_autos", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3151_pid_cvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3151_pid_prog_opers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3151_pid_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3151_pid_sps", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3151_upper_position_limits", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3152_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3153_pid_autos", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3153_pid_cvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3153_pid_prog_opers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3153_pid_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3153_pid_sps", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3153_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3154_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3156_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3158_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3161_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3163_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3165_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3166_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3167_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3170_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3171_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3174_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3175_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3181_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3186_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3187_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3188_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3190_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3192_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3194_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3197_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3198_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3201_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3204_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3205_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3207_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3208_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3209_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3214_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3215_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3216_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3217_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3218_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3220_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3221_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3222_actual_set_points", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3222_actual_speeds", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3222_modes", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3222_pid_autos", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3222_pid_cvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3222_pid_prog_opers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3222_pid_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3222_pid_sps", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3222_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3223_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3224_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3226_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3227_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3228_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3231_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3236_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3237_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3238_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3239_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3240_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3241_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3242_actual_set_points", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3242_actual_speeds", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3242_modes", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3242_pid_autos", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3242_pid_cvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3242_pid_prog_opers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3242_pid_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3242_pid_sps", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3242_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3243_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3265_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3267_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3269_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3273_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3279_runnings", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3280_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_l3285_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_lxxx2_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_lxxx3_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_lxxxx_statuses", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_lxxxy_pvs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_int_no1_filter_no_bws", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_int_no1_filter_total_bws", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_int_no1_filter_total_cebs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_int_no2_filter_no_bws", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_int_no2_filter_total_bws", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_int_no2_filter_total_cebs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_req_enables", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_sp_air_scour_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_sp_back_wash_recipes", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_sp_ceb_recipes", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_sp_no_bw_before_cebs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_sp_soak_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_step_numbers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_step_previous", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_step_resets", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_clean_seq_step_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_int_no1_filter_pdt_decay_rates", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_int_no1_filter_pdt_start_pressures", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_int_no1_filter_pdt_stop_pressures", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_int_no1_filter_tmps", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_int_no1_filter_volumes", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_int_no2_filter_pdt_decay_rates", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_int_no2_filter_pdt_start_pressures", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_int_no2_filter_pdt_stop_pressures", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_int_no2_filter_tmps", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_int_no2_filter_volumes", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_req_enables", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_sp_max_pdt_decay_rates", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_sp_max_tmps", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_sp_max_turbidities", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_sp_max_volumes", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_sp_pdt_decay_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_step_numbers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_step_previous", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_step_resets", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_mf_seq_step_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_o3_seq_int_processed_volumes", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_o3_seq_req_enables", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_o3_seq_sp_discharge_concs", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_o3_seq_sp_ozone_consumptions", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_o3_seq_step_numbers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_o3_seq_step_previous", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_o3_seq_step_resets", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_o3_seq_step_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_oa_seq_req_enables", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_oa_seq_sp_batch_start_levels", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_oa_seq_sp_batch_stop_levels", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_oa_seq_sp_feed_flows", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_oa_seq_step_numbers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_oa_seq_step_previous", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_oa_seq_step_resets", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_oa_seq_step_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_int_acid_recipe_nos", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_int_caustic_recipe_nos", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_int_pdt_decay_rates", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_int_pdt_start_pressures", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_int_pdt_stop_pressures", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_int_preservation_recipe_nos", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_req_enables", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_sp_acid_wash_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_sp_caustic_wash_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_sp_concentrate_conductivities", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_sp_concentrate_flows", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_sp_mix_tank_op_levels", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_sp_pdt_decay_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_sp_recoveries", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_step_numbers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_step_previous", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_step_resets", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_ro_seq_step_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_uv_seq_reactor_no1_dose_values", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_uv_seq_reactor_no2_dose_values", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_uv_seq_reactor_no3_dose_values", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_uv_seq_req_enables", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_uv_seq_sp_min_run_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_uv_seq_sp_min_uv_intensities", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_uv_seq_step_numbers", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_uv_seq_step_previous", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_uv_seq_step_resets", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_uv_seq_step_times", force: true do |t|
    t.float    "value"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trends", force: true do |t|
    t.string "name",       limit: 79
    t.string "expr",       limit: 254
    t.string "trig",       limit: 254
    t.string "sampleper",  limit: 16
    t.string "priv",       limit: 16
    t.string "area",       limit: 16
    t.string "eng_units",  limit: 8
    t.string "format",     limit: 11
    t.string "filename",   limit: 253
    t.string "files",      limit: 4
    t.string "time",       limit: 32
    t.string "period",     limit: 32
    t.string "comment",    limit: 48
    t.string "trendtype",  limit: 32
    t.string "spcflag",    limit: 4
    t.string "lsl",        limit: 16
    t.string "usl",        limit: 16
    t.string "subgrpsize", limit: 8
    t.string "xdoublebar", limit: 16
    t.string "range",      limit: 16
    t.string "sdeviation", limit: 16
    t.string "stormethod", limit: 64
    t.string "cluster",    limit: 16
    t.string "taggenlink", limit: 32
  end

  create_table "turbidities", force: true do |t|
    t.integer  "equipment_id"
    t.datetime "Formazin_Calibration_Date"
    t.string   "Formazin_Comments"
    t.datetime "Gelex_Check_Date"
    t.string   "Gelex_Comments"
    t.string   "Other_Comments"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "turbidities", ["equipment_id"], name: "index_turbidities_on_equipment_id"

  create_table "turbidity_records", force: true do |t|
    t.integer  "turbidity_id"
    t.string   "Type"
    t.integer  "Calibration_Check"
    t.float    "Range_Reading"
    t.boolean  "Tick"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "turbidity_records", ["turbidity_id"], name: "index_turbidity_records_on_turbidity_id"

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "authentication_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin",                  default: false
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "variables", force: true do |t|
    t.string  "name",         limit: 79
    t.string  "variabletype", limit: 16
    t.string  "unit",         limit: 31
    t.string  "addr",         limit: 254
    t.string  "raw_zero",     limit: 11
    t.integer "raw_full"
    t.string  "eng_zero",     limit: 11
    t.integer "eng_full"
    t.string  "eng_units",    limit: 8
    t.string  "format",       limit: 11
    t.string  "comment",      limit: 48
    t.string  "editcode",     limit: 8
    t.string  "linked",       limit: 1
    t.string  "oid",          limit: 10
    t.string  "ref1",         limit: 11
    t.string  "ref2",         limit: 11
    t.string  "deadband",     limit: 11
    t.string  "custom",       limit: 128
    t.string  "taggenlink",   limit: 32
    t.string  "cluster",      limit: 16
  end

end
