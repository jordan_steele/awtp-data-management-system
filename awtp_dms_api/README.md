# AWTP Data Management System API

This project is responsible for storing the data needed for the project and providing an API to access it. It has been built using [Ruby on Rails][rails]. 

## Getting Started

To get you started you need to install the dependencies:

### Prerequisites
* [Ruby][ruby]
* [Ruby on Rails][rails]
* [Bundler][bundler]

### Install Dependencies

Dependencies are managed by [Bundler][bundler] and can be installed by running:

```
bundle install
```

### Database Creation

To create the database run:

```
rake db:migrate
```

### Database Initialization

To seed the database run:

```
rake db:seed
```

### Run the Application

Simply run:

```
rails server
```

Now browse to the api at `http://localhost:3000/api/v1/`.


## Directory Layout

The layout is very similar to most rails apps! Online resources will fill in the blanks.
TODO: Actually work out which folders are needed, most are rails auto generated and we are only building an API

    app/
      assets/                               
      controllers/                            --> Controllers
        api/                                  --> Folder structure follows routes
          v1/
            a_controller.rb                   --> controller for a model
            another_controller.rb
      helpers/
      mailers/
      models/
        a_model.rb                            --> Define what makes attributes valid here + functions on model
        ...
      views/                                  --> Views of models, rails object -> json
        api/                                  --> Folder structure follows routes
          v1/
            someModel/
              _show.json.jbuilder             --> Make partials to be used in other jbuilder files
              {create,delete,destroy,index,show,update}.json.jbuilder
            someModel2/
    bin/
    config/
      routes.rb                               --> Define routes here
      ...
    db/
      seeds.rb                                --> Define data for seeding database here
      ...
    lib/
    log/
    public/
    spec/                                     --> Rspec files for testing go here!
      controllers/
        api/                                  --> Folder structure follows routes
          v1/
            a_controller_spec.rb              --> spec for a controller
            another_controller_spec.rb
      factories/
        a_model.rb                            --> Factory for a model
      models/
        a_model_spec.rb                       --> spec for a model
      support/
        helper1.rb
    tmp/
    vendor/
    Gemfile                                   --> Define module dependencies here!
    Gemfile.lock
    Rakefile                                  --> Define 'tasks' here to ease development


## Testing

We are using [RSpec][rspec] for BDD testing. All tests are written following the [RSpec][rspec] format. 

We are also using [Factory Girl][factorygirl] as a replacement for fixtures.

The two main kinds of tests that we will be using for testing our API

* Model tests, contained in `spec/models/` are used to test that only the correct kind of records can be made and to test any functions on our models.
* Controller tests, contained in `spec/controllers`. These are integration testing and will test the URL endpoints and determine if the correct DB actions happen and if the JSON and response codes are correct.

### Running Tests

To run the tests simply run:

```
bundle exec rspec
```


[ruby]: https://www.ruby-lang.org/en/
[rails]: http://rubyonrails.org/
[bundler]: http://bundler.io/
[rspec]: http://rspec.info/
[factorygirl]: https://github.com/thoughtbot/factory_girl_rails