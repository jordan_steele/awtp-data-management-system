json.info "Logged in"
json.user do
  json.extract! @user, :id, :email, :authentication_token, :admin
end