json.merge! trend.attributes
if defined? data_points
  json.data_points data_points do |data_points|
    json.value data_points.value
    json.time data_points.time
  end
else
end