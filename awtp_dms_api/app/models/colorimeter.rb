class Colorimeter < ActiveRecord::Base
  belongs_to :equipment

  validates :equipment_id, :comments, presence: true
end
