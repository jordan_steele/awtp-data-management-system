class Equipment < ActiveRecord::Base
  has_many :colorimeters
  has_many :ph_calibrations
end
