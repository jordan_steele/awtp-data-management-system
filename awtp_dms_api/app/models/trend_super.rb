class TrendSuper < ActiveRecord::Base
  self.abstract_class = true

  validates :value, :time, presence: true
end
