class ApplicationController < ActionController::Base

  # define which model will act as token authenticatable
  acts_as_token_authentication_handler_for User
  
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  skip_before_filter :verify_authenticity_token, if: :json_request?
  
  private

    def json_request?
      request.format.json?
    end

    def is_admin?
      if (current_user and !current_user.admin?) or !current_user
        render :json => { 
          :error => "You do not have appropriate access to this method"
        }, :status => :unauthorized
      else
        true
      end
    end

end
