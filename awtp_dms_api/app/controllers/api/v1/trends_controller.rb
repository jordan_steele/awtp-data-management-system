class Api::V1::TrendsController < ApplicationController

  api :GET, '/trends/:id', "Get trend with id"
  formats ['json']
  example TREND_EXAMPLE_SHOW
  param :id, /^[0-9]*/, :desc => "Id of trend to fetch", :required => true
  param :from_date, /^[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,2}[ [0-9]{0,2}:[0-9]{0,2}]{0,1}/, :desc => "Start date for data points", :required => false
  param :to_date, /^[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,2}[ [0-9]{0,2}:[0-9]{0,2}]{0,1}/, :desc => "End date for data points", :required => false
  def show
    @trend = Trend.find(params[:id])
    trendmodel = get_trend_model(@trend)
    if params[:from_date] == nil or params[:to_date] == nil
      @data_points = trendmodel.all
    else
      @data_points = trendmodel.where(time: date_from_string(params[:from_date])..date_from_string(params[:to_date]))
    end
    if @data_points.length > MAX_DATA_POINTS
      every_n = @data_points.length / MAX_DATA_POINTS
      @data_points = array_mod(@data_points.to_a, every_n)
    end
  end

  api :GET, '/trends/', "Get all trends"
  formats ['json']
  example TREND_EXAMPLE_INDEX
  param :name, String, :desc => "Search value for name attribute on trends", :required => false
  def index
    trends = Trend.arel_table
    @trends = Trend.where(trends[:name].matches("%#{params[:name]}%"))
  end

  private
    def get_trend_model(trend)
      return ("Trend" + trend.name.underscore.camelize).constantize
    end

    def array_mod(arr, mod, offset = 0)
      arr.shift(offset)
      out_arr = []

      arr.each_with_index do |val, idx|
        out_arr << val if idx % mod == 0
      end

      out_arr
    end

    def date_from_string(string)
      split = string.split('-')
      year = split[0]
      month = split[1]
      day = split[2]
      return Date.parse("#{day}-#{month}-#{year}")
    end

end
