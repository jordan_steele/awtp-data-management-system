require 'json';

class Api::V1::UsersController < ApplicationController
  acts_as_token_authentication_handler_for User, :except => [:create]
  before_filter :is_admin?, :only => [:index, :update, :destroy]

  api :POST, '/users/', "Create new user"
  formats ['json']
  example USERS_EXAMPLE
  param :user, Hash, :desc => "User info" do
    param :email, String, :desc => "Email address to create account with", :required => true
    param :password, String, :desc => "Password to create account with", :required => true
    param :password_confirmation, String, :desc => "Copy of password field", :required => true
  end
  def create
    @user = User.new(user_params(true))
    
    if !@user.save
      render :json => { :error => @user.errors }, :status => :bad_request
    end
  end

  api :PUT, '/users/:id', "Update a current user"
  formats ['json']
  example USERS_EXAMPLE
  param :user, Hash, :desc => "User info" do
    param :email, String, :desc => "Email address to create account with", :required => true
    param :password, String, :desc => "Password to create account with", :required => true
    param :password_confirmation, String, :desc => "Copy of password field", :required => true
  end
  def update
    @user = User.find(params[:id])
    
    if !@user.update(user_params)
      render :json => { :error => @user.errors }, :status => :bad_request
    end
  end

  api :GET, '/users/:id', "Get user with id"
  formats ['json']
  example USERS_EXAMPLE
  param :id, /^[0-9]*/, :desc => "Id of user to fetch", :required => true
  def show
    @user = User.find(params[:id])
  end

  api :GET, '/users/', "Get all users"
  formats ['json']
  example USERS_INDEX_EXAMPLE
  def index
    @users = User.all
  end

  api :DELETE, '/users/', "Delete a user with id"
  formats ['json']
  param :id, /^[0-9]*/, :desc => "Id of user to delete", :required => true
  def destroy
    @user = User.find(params[:id])
    @user.destroy
  end

  private
  def user_params(create=false)
    if params[:user][:password] == nil and params[:user][:password_confirmation] == nil
      params[:user][:password] = params[:password]
      params[:user][:password_confirmation] = params[:password_confirmation]
    end
    if create
      params.require(:user).permit(:email, :password, :password_confirmation)
    else 
      params.require(:user).permit(:id, :email, :password, :password_confirmation, :admin)
    end
  end
end
