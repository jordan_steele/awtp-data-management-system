class Api::V1::AlarmsController < ApplicationController
  
  api :GET, '/alarms/', "Get all alarms"
  formats ['json']
  example ALARM_EXAMPLE_INDEX
  param :tag, String, :desc => "Search value for tag attribute on alarms", :required => false
  param :name, String, :desc => "Search value for name attribute on alarms", :required => false
  param :desc, String, :desc => "Search value for desc attribute on alarms", :required => false
  def index
    digalms = Digalm.arel_table
    query_parts = []
    [:tag, :name, :desc].each do |fld|
      query_parts << digalms[fld].matches("%#{params[fld]}%") if params[fld].present?
    end
    @digalms = Digalm.where(query_parts.reduce(&:and))
  end
  
  api :GET, '/alarms/:id', "Get alarm with id"
  formats ['json']
  example ALARM_EXAMPLE_SHOW
  param :id, /^[0-9]*/, :desc => "Id of alarm to fetch", :required => true
  def show
    @digalm = Digalm.find(params[:id])
  end

end