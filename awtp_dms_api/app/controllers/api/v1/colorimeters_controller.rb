class Api::V1::ColorimetersController < Api::V1::OperatorFormsController

  api :POST, '/colorimeters/', "Create new colorimeter record"
  formats ['json']
  example COLORIMETERS_EXAMPLE
  param :colorimeter, Hash, :desc => "Colorimeter info" do
    param :equipment_id, Integer, :desc => "ID of equipment this data refers to", :required => true
    param :date, /^[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,2}[ [0-9]{0,2}:[0-9]{0,2}]{0,1}/, :desc => "The data that the data was recorded", :required => true
    param :analyst, String, :desc => "The person that recorded the data", :required => true
    param :type, String, :desc => "???", :required => true
    param :sol1, String, :desc => "???", :required => true
    param :sol2, String, :desc => "???", :required => true
    param :sol3, String, :desc => "???", :required => true
    param :comments, String, :desc => "Pass/fail and date for next check", :required => true
  end
  def create
    super
  end

  api :PUT, '/colorimeters/:id', "Update a colorimeter record"
  formats ['json']
  example COLORIMETERS_EXAMPLE
  param :colorimeter, Hash, :desc => "Colorimeter info" do
    param :equipment_id, Integer, :desc => "ID of equipment this data refers to"
    param :date, /^[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,2}[ [0-9]{0,2}:[0-9]{0,2}]{0,1}/, :desc => "The data that the data was recorded"
    param :analyst, String, :desc => "The person that recorded the data"
    param :type, String, :desc => "???"
    param :sol1, String, :desc => "???"
    param :sol2, String, :desc => "???"
    param :sol3, String, :desc => "???"
    param :comments, String, :desc => "Pass/fail and date for next check"
  end
  def update
    super
  end

  api :GET, '/colorimeters/:id', "Get colorimeter record with id"
  formats ['json']
  example COLORIMETERS_EXAMPLE
  param :id, /^[0-9]*/, :desc => "Id of colorimeter record to fetch", :required => true
  def show
    super
  end

  api :GET, '/colorimeters/', "Get all colorimeter records"
  formats ['json']
  example COLORIMETERS_INDEX_EXAMPLE  
  def index
    super
  end

  api :DELETE, '/colorimeters/', "Delete a colorimeter record with id"
  formats ['json']
  param :id, /^[0-9]*/, :desc => "Id of colorimeter record to delete", :required => true
  def destroy
    super
  end

  protected
    def form_params
      params.require(:colorimeter).permit(:equipment_id, :date, :analyst, :type,
                                          :sol1, :sol2, :sol3, :comments)
    end

end
