class Api::V1::EquipmentsController < Api::V1::OperatorFormsController

  def create
    super
  end

  def update
    super
  end

  def show
    super
  end

  def index
    super
  end

  def destroy
    super
  end

  protected
    def form_params
      params.require(:equipment).permit(:location, :model, :equipment_number, 
                                        :serial_number, :make)
    end

end