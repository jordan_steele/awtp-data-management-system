class Api::V1::SessionsController < Devise::SessionsController

  prepend_before_filter :verify_signed_out_user, :except => [:create, :destroy]
  skip_before_filter :verify_authenticity_token, if: :json_request?
  skip_before_filter :authenticate_entity_from_token!

  api :POST, '/sessions/', "Create new session (login)"
  formats ['json']
  example SESSIONS_CREATE_SUCCESS_EXAMPLE
  example SESSIONS_CREATE_FAILURE_EXAMPLE
  param :user, Hash, :desc => "User info" do
    param :email, String, :desc => "Email of account to login to", :required => true
    param :password, String, :desc => "Password of account to login to", :required => true
  end
  def create
    warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#failure")
    @user = current_user
  end

  api :DELETE, '/sessions/', "Destroy a session (logout)"
  formats ['json']
  example SESSIONS_DESTROY_SUCCESS_EXAMPLE
  example SESSIONS_DESTROY_FAILURE_EXAMPLE
  def destroy
    warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#failure")
    current_user.update_column(:authentication_token, nil)
    render :json => { :info => "Logged out" }, :status => 200
  end

  def failure
    render :json => { :error => "Login Credentials Failed" }, :status => 401
  end
end
