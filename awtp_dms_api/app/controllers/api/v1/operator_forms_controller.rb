class Api::V1::OperatorFormsController < ApplicationController

  before_filter :is_admin?

  def create
    @form = form_model.new(form_params)

    if !@form.save
      render :json => { :error => @form.errors }, :status => :bad_request
    end
  end

  def update
    @form = form_model.find(params[:id])

    if !@form.update(form_params)
      render :json => { :error => @user.errors }, :status => :bad_request
    end
  end

  def show
    @form = form_model.find(params[:id])
  end

  def index
    @forms = form_model.all
  end

  def destroy
    @form = form_model.find(params[:id])
    @form.destroy
  end

  protected
    def form_params
    end

  private
    def form_model
      controller_name.singularize.camelize.constantize
    end

end
