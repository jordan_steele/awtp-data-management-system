class Api::V1::PhCalibrationsController < Api::V1::OperatorFormsController

  api :POST, '/p_h_calibrations/', "Create new p_h_calibration record"
  formats ['json']
  example PHCALIBRATION_EXAMPLE
  param :p_h_calibration, Hash, :desc => "p_h_calibration info" do
    param :equipment_id, Integer, :desc => "ID of equipment this data refers to", :required => true
    param :date, /^[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,2}[ [0-9]{0,2}:[0-9]{0,2}]{0,1}/, :desc => "The data that the data was recorded", :required => true
    param :analyst, String, :desc => "The person that recorded the data", :required => true
    param :change_of_buffers, [true, false], :desc => "Whether the buffers have been changed or not", :required => true
    param :check_or_calibration, /^(CK|CAL)/, :desc => "Whether this is a check or calibration", :required => true
    param :buffer_calibration, /^(pH 4|pH 7|pH 10)/, :desc => "Whether this buffer was used or not", :required => true
    param :slope, String, :desc => "(-58 +/-3mV) at 25OC", :required => true
    param :comments, String, :desc => "Pass/fail and date for next check", :required => true
  end
  def create
    super
  end

  api :PUT, '/p_h_calibrations/:id', "Update a p_h_calibration record"
  formats ['json']
  example PHCALIBRATION_EXAMPLE
  param :p_h_calibration, Hash, :desc => "p_h_calibration info" do
    param :equipment_id, Integer, :desc => "ID of equipment this data refers to"
    param :date, /^[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,2}[ [0-9]{0,2}:[0-9]{0,2}]{0,1}/, :desc => "The data that the data was recorded"
    param :analyst, String, :desc => "The person that recorded the data"
    param :change_of_buffers, [true, false], :desc => "Whether the buffers have been changed or not"
    param :check_or_calibration, /^CK|CAL/, :desc => "Whether this is a check or calibration"
    param :buffer_calibration, /^(pH 4|pH 7|pH 10)/, :desc => "Whether this buffer was used or not"
    param :slope, String, :desc => "(-58 +/-3mV) at 25OC"
    param :comments, String, :desc => "Pass/fail and date for next check"
  end
  def update
    super
  end

  api :GET, '/p_h_calibrations/:id', "Get p_h_calibration record with id"
  formats ['json']
  example PHCALIBRATION_EXAMPLE
  param :id, /^[0-9]*/, :desc => "Id of p_h_calibration record to fetch", :required => true
  def show
    super
  end

  api :GET, '/p_h_calibrations/', "Get all p_h_calibration records"
  formats ['json']
  example PHCALIBRATION_INDEX_EXAMPLE  
  def index
    super
  end

  api :DELETE, '/p_h_calibrations/', "Delete a p_h_calibration record with id"
  formats ['json']
  param :id, /^[0-9]*/, :desc => "Id of p_h_calibration record to delete", :required => true
  def destroy
    super
  end

  protected
    def form_params
      params.require(:ph_calibration).permit(:equipment_id, :date, :analyst, :change_of_buffers,
                                          :check_or_calibration, :buffer_calibration,
                                          :slope, :comments)
    end

end
