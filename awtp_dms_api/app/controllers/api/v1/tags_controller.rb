class Api::V1::TagsController < ApplicationController

  def create
    @tag = Tag.new(tag_params)
    
    if !@tag.save
      render :status => :bad_request
    end
  end

  def update
    @tag = Tag.find(params[:id])
    
    if !@tag.update(tag_params)
      render :status => :bad_request
    end
  end

  def show
    @tag = Tag.find(params[:id])
  end

  def index
    @tags = Tag.all
  end

  def destroy
    @tag = Tag.find(params[:id])
    @tag.destroy
  end

  private
    def tag_params
      params.require(:tag).permit(:name, :value)
    end

end
