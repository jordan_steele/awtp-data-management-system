require 'rails_helper'

RSpec.describe Api::V1::SessionsController, :type => :controller do
  render_views

  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  describe "POST #create" do
    before(:each) do
      @user = FactoryGirl.create(:user)
    end

    context "with valid account credentials" do
      before(:each) do
        user = {
          "email" => @user.email,
          "password" => @user.password
        }
        post :create, user: user, :format => :json
      end

      it "updates user with new authentication token" do
        expect(json['user']['authentication_token']).to eq(@user.authentication_token)
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end

    context "with invalid account credentials" do
      it "does not update user with new authenticatino token" do
        user = {
          "email" => @user.email,
          "password" => @user.password + "asd"
        }
        expect{
          post :create, user: user, :format => :json
        }.to_not change{@user.authentication_token}
      end

      it "responds with bad request" do
        user = {
          "email" => @user.email,
          "password" => @user.password + "asd"
        }
        post :create, user: user, :format => :json
        expect(response.response_code).to eq 401
      end
    end
  end

  describe "DELETE #destroy" do
    login_user

    before(:each) do
      delete :destroy, :format => :json
    end

    it "log the user out" do
      expect(json['info']).to eq("Logged out")
    end

    it "responds with success" do
      expect(response).to be_success
    end
  end

end