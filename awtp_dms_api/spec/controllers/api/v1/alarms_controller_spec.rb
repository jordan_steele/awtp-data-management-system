require 'rails_helper'

RSpec.describe Api::V1::AlarmsController, :type => :controller do
  render_views

  before { controller.stub(:authenticate_user!).and_return true }

  describe "GET #index" do
    before(:each) do
      FactoryGirl.create_list(:digalm, 5, tag: "atag")
      FactoryGirl.create_list(:digalm, 5, name: "aname")
      FactoryGirl.create_list(:digalm, 5, desc: "adesc")
      FactoryGirl.create_list(:digalm, 5, tag: "btag", name: "bname", desc: "bdesc")
    end
    
    context "with no search terms" do
      before(:each) do 
        get :index, :format => :json
      end

      it "sends a list of all the alarms" do
        expect(json.length).to eq(20)
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end

    context "with search term for tag" do
      before(:each) do
        get :index, tag: "atag", :format => :json
      end

      it "sends a list of all the alarms with the tag term" do
        expect(json.length).to eq(5)
        expect(json[0]["tag"]).to eq("atag")
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end

    context "with search term for name" do
      before(:each) do
        get :index, name: "aname", :format => :json
      end

      it "sends a list of all the alarms with the name term" do
        expect(json.length).to eq(5)
        expect(json[0]["name"]).to eq("aname")
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end

    context "with search term for desc" do
      before(:each) do
        get :index, desc: "adesc", :format => :json
      end

      it "sends a list of all the alarms with the desc term" do
        expect(json.length).to eq(5)
        expect(json[0]["desc"]).to eq("adesc")
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end

    context "with a search term for tag, name and desc" do
      before(:each) do
        get :index, tag: "btag", name: "bname", desc: "bdesc", :format => :json
      end

      it "sends a list of all the alarms with all the search terms" do
        expect(json.length).to eq(5)
        expect(json[0]["tag"]).to eq("btag")
        expect(json[0]["name"]).to eq("bname")
        expect(json[0]["desc"]).to eq("bdesc")
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end

  end

  describe "GET #show" do
    before(:each) do
      @alarm = FactoryGirl.create(:digalm)
      get :show , :id => @alarm.id , :format => :json
    end
    
    it "should respond successfully" do
      expect(response).to be_success
    end

    it "should find digalm with id" do
      expect(json).to have_key('name')
    end
  end

end
