require 'rails_helper'

RSpec.describe Api::V1::TagsController, :type => :controller do
  render_views

  before { controller.stub(:authenticate_user!).and_return true }

  describe "GET #index" do
    before(:each) do
      FactoryGirl.create_list(:tag, 10)
      get :index, :format => :json
    end

    it "sends a list of all the tags" do
      expect(json.length).to eq(10)
    end

    it "respons with success" do
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    before(:each) do
      @tag = FactoryGirl.create(:tag)
      get :show, :id => @tag.id, :format => :json
    end

    it "responds with success" do
      expect(response).to be_success
    end

    it "responds with the correct name" do
      expect(json['name']).to eq(@tag.name)
    end

    it "responds with the correct value" do
      expect(json['value']).to eq(@tag.value)
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new tag in the database" do
        expect{
          post :create, tag: FactoryGirl.attributes_for(:tag), :format => :json
        }.to change(Tag, :count).by(1)
      end

      it "responds with success" do
        post :create, tag: FactoryGirl.attributes_for(:tag), :format => :json
        expect(response).to be_success
      end
    end

    context "with invalid attributes" do
      it "does not save the new contact in the database" do
        expect{
          post :create, tag: FactoryGirl.attributes_for(
            :tag, name: nil), :format => :json
        }.to_not change(Tag, :count)
      end

      it "responds with bad request" do
        post :create, tag: FactoryGirl.attributes_for(
          :tag, name: nil), :format => :json
        expect(response).to be_bad_request
      end
    end
  end

  describe "PUT #update" do
    before(:each) do
      @tag = FactoryGirl.create(:tag, name: "tag0002", value: 10)
    end

    context "with valid attributes" do
      before(:each) do
        put :update, id: @tag.id, tag: FactoryGirl.attributes_for(
          :tag, name: "tag0003", value:12), :format => :json
      end

      it "located the requested tag" do
        expect(assigns(:tag)).to eq(@tag)
      end

      it "changes the tag's attributes" do
        @tag.reload
        expect(@tag.name).to eq("tag0003")
        expect(@tag.value).to eq(12)
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end

    context "with invalid attributes" do
      before(:each) do
        put :update, id: @tag.id, tag: FactoryGirl.attributes_for(
          :tag, name: nil), :format => :json
      end

      it "locates the requested tag" do
        expect(assigns(:tag)).to eq(@tag)
      end

      it "does not change the tag's attributes" do
        @tag.reload
        expect(@tag.name).to eq("tag0002")
        expect(@tag.value).to eq(10)
      end

      it "responds with bad request" do
        expect(response).to be_bad_request
      end
    end
  end

  describe "DELETE #destroy" do
    before(:each) do
      @tag = FactoryGirl.create(:tag)
    end

    it "deletes the tag" do
      expect {
        delete :destroy, id: @tag.id, :format => :json
      }.to change(Tag, :count).by(-1)
    end

    it "responds with success" do
      expect(response).to be_success
    end
  end

end
