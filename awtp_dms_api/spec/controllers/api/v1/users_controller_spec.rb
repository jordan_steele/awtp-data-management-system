require 'rails_helper'

RSpec.describe Api::V1::UsersController, :type => :controller do
  render_views

  describe "GET #index" do
    before(:each) do
      FactoryGirl.create_list(:user, 10)
    end

    context "as a guest" do
      before(:each) do
        get :index, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as a regular user" do
      login_user
      before(:each) do
        get :index, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as an admin" do
      login_admin
      before(:each) do
        get :index, :format => :json
      end

      it "sends a list of all the users" do
        expect(json.length).to eq(11)
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end
  end

  describe "GET #show" do
    before(:each) do
      @user = FactoryGirl.create(:user)
    end

    context "as a guest" do
      before(:each) do
        get :show, :id => @user.id, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as a regular user" do
      login_user
      before(:each) do
        get :show, :id => @user.id, :format => :json
      end

      it "responds with success" do
        expect(response).to be_success
      end

      it "responds with the correct email" do
        expect(json['email']).to eq(@user.email)
      end
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new user in the database" do
        expect{
          post :create, user: FactoryGirl.attributes_for(:user), :format => :json
        }.to change(User, :count).by(1)
      end

      it "responds with success" do
        post :create, user: FactoryGirl.attributes_for(:user), :format => :json
        expect(response).to be_success
      end
    end

    context "with invalid attributes" do
      it "does not save the new user in the database" do
        expect{
          post :create, user: FactoryGirl.attributes_for(
            :user, email: "123"), :format => :json
        }.to_not change(User, :count)
      end

      it "responds with bad request" do
        post :create, user: FactoryGirl.attributes_for(
          :user, email: "123"), :format => :json
        expect(response).to be_bad_request
      end
    end
  end

  describe "PUT #update" do
    before(:each) do
      @user = FactoryGirl.create(:user, admin: false)
    end

    context "as a guest" do
      before(:each) do
        put :update, id: @user.id, user: FactoryGirl.attributes_for(
            :user, admin: true), :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as a regular user" do
      login_user
      before(:each) do
        put :update, id: @user.id, user: FactoryGirl.attributes_for(
            :user, admin: true), :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as an admin" do 
      login_admin

      context "with valid attributes" do
        before(:each) do
          put :update, id: @user.id, user: FactoryGirl.attributes_for(
            :user, admin: true), :format => :json
        end

        it "located the requested user" do
          expect(assigns(:user)).to eq(@user)
        end

        it "changes the user's attributes" do
          @user.reload
          expect(@user.admin).to eq(true)
        end

        it "responds with success" do
          expect(response).to be_success
        end
      end

      context "with invalid attributes" do
        before(:each) do
          put :update, id: @user.id, user: FactoryGirl.attributes_for(
            :user, email: "123", admin: true), :format => :json
        end

        it "locates the requested user" do
          expect(assigns(:user)).to eq(@user)
        end

        it "does not change the tag's attributes" do
          @user.reload
          expect(@user.admin).to eq(false)
        end

        it "responds with bad request" do
          expect(response).to be_bad_request
        end
      end
    end
  end

  describe "DELETE #destroy" do
    before(:each) do
      @user = FactoryGirl.create(:user)
    end

    context "as a guest" do
      before(:each) do
        delete :destroy, id: @user.id, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as a regular user" do
      login_user
      before(:each) do
        delete :destroy, id: @user.id, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as an admin" do 
      login_admin

      it "deletes the user" do
        expect {
          delete :destroy, id: @user.id, :format => :json
        }.to change(User, :count).by(-1)
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end
  end

end
