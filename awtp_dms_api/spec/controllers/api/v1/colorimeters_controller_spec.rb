require 'rails_helper'

RSpec.describe Api::V1::ColorimetersController, :type => :controller do
  render_views
  login_admin

  describe "GET #index" do
    before(:each) do
      FactoryGirl.create_list(:colorimeter, 10)
      get :index, :format => :json
    end

    it "sends a list of all the colorimeters" do
      expect(json.length).to eq(10)
    end

    it "responds with success" do
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    before(:each) do
      @colorimeter = FactoryGirl.create(:colorimeter)
      get :show, :id => @colorimeter.id, :format => :json
    end

    it "responds with success" do
      expect(response).to be_success
    end

    it "responds with the correct equipment id" do
      expect(json['equipment_id']).to eq(@colorimeter.equipment_id)
    end

    it "responds with the correct sol1" do
      expect(json['sol1']).to eq(@colorimeter.sol1)
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new colorimeter in the database" do
        expect{
          post :create, colorimeter: FactoryGirl.attributes_for(:colorimeter), :format => :json
        }.to change(Colorimeter, :count).by(1)
      end

      it "responds with success" do
        post :create, colorimeter: FactoryGirl.attributes_for(:colorimeter), :format => :json
        expect(response).to be_success
      end
    end

    context "with invalid attributes" do
      it "does not save the new colorimeter in the database" do
        expect{
          post :create, colorimeter: FactoryGirl.attributes_for(
            :colorimeter, equipment_id: nil), :format => :json
        }.to_not change(Colorimeter, :count)
      end

      it "responds with bad request" do
        post :create, colorimeter: FactoryGirl.attributes_for(
            :colorimeter, equipment_id: nil), :format => :json
        expect(response).to be_bad_request
      end
    end
  end

  describe "PUT #update" do
    before(:each) do
      @colorimeter = FactoryGirl.create(:colorimeter, analyst: "Jason")
    end

    context "with valid attributes" do
      before(:each) do
        put :update, id: @colorimeter.id, colorimeter: FactoryGirl.attributes_for(
          :colorimeter, analyst: @colorimeter.analyst + " Jack"), :format => :json
      end

      it "located the requested colorimeter" do
        expect(assigns(:form)).to eq(@colorimeter)
      end

      it "changes the colorimeter's attributes" do
        @colorimeter.reload
        expect(@colorimeter.analyst).to eq("Jason Jack")
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end

    # context "with invalid attributes" do
    #   before(:each) do
    #     # post :create, colorimeter: FactoryGirl.attributes_for(
    #     #     :colorimeter, equipment_id: nil), :format => :json
    #     put :update, id: @colorimeter.id, colorimeter: FactoryGirl.attributes_for(
    #       :colorimeter, Comments: ""), 
    #       :format => :json
    #   end

    #   it "locates the requested colorimeter" do
    #     expect(assigns(:form)).to eq(@colorimeter)
    #   end

    #   it "does not change the colorimeter's attributes" do
    #     @colorimeter.reload
    #     expect(@colorimeter.analyst).to eq("Jason")
    #   end

    #   it "responds with bad request" do
    #     expect(response).to be_bad_request
    #   end
    # end
  end

  describe "DELETE #destroy" do
    before(:each) do
      @colorimeter = FactoryGirl.create(:colorimeter)
    end

    it "deletes the colorimeter" do
      expect {
        delete :destroy, id: @colorimeter.id, :format => :json
      }.to change(Colorimeter, :count).by(-1)
    end

    it "responds with success" do
      expect(response).to be_success
    end
  end

end
