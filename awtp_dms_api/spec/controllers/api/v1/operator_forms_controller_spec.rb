require 'rails_helper'

RSpec.describe Api::V1::OperatorFormsController, :type => :controller do
  render_views

  controller(Api::V1::OperatorFormsController) do
    def index; render :text => "nothing"; end
    def show; render :text => "nothing"; end
    def create; render :text => "nothing"; end
    def update; render :text => "nothing"; end
    def destroy; render :text => "nothing"; end
  end

  describe "GET #index" do
    context "as a guest" do
      before(:each) do
        get :index, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as a regular user" do 
      login_user
      before(:each) do
        get :index, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as an admin" do 
      login_admin
      before(:each) do 
        get :index, :format => :json
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end
  end

  describe "GET #show" do
    context "as a guest" do
      before(:each) do
        get :show, id: 1, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as a regular user" do 
      login_user
      before(:each) do
        get :show, id: 1, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as an admin" do 
      login_admin
      before(:each) do 
        get :show, id: 1, :format => :json
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end
  end

  describe "POST #create" do
    context "as a guest" do
      before(:each) do
        post :create, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as a regular user" do 
      login_user
      before(:each) do
        post :create, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as an admin" do 
      login_admin
      before(:each) do 
        post :create, :format => :json
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "as a guest" do
      before(:each) do
        put :update, id: 1, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as a regular user" do 
      login_user
      before(:each) do
        put :update, id: 1, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as an admin" do 
      login_admin
      before(:each) do 
        put :update, id: 1, :format => :json
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    context "as a guest" do
      before(:each) do
        delete :destroy, id: 1, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as a regular user" do 
      login_user
      before(:each) do
        delete :destroy, id: 1, :format => :json
      end

      it "responds with unauthorized" do
        expect(response.response_code).to eq(401)
      end
    end

    context "as an admin" do 
      login_admin
      before(:each) do 
        delete :destroy, id: 1, :format => :json
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end
  end

end
