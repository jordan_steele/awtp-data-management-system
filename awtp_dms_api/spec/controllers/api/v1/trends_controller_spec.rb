require 'rails_helper'

RSpec.describe Api::V1::TrendsController, :type => :controller do
  render_views

  before { controller.stub(:authenticate_user!).and_return true }

  describe "GET #index" do
    before(:each) do
      FactoryGirl.create_list(:trend, 5, name: "hello")
      FactoryGirl.create_list(:trend, 5, name: "goodbye")
    end
    
    context "with no search term for name" do
      before(:each) do
        get :index, :format => :json
      end

      it "sends a list of all the trends" do
        expect(json.length).to eq(10)
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end

    context "with search term for name" do
      before(:each) do
        get :index, name: "hello", :format => :json
      end

      it "sends a list of all the trends with the search term as name" do
        expect(json.length).to eq(5)
        expect(json[0]["name"]).to eq("hello")
      end

      it "responds with success" do
        expect(response).to be_success
      end
    end
  end

  describe "GET #show" do
    before(:each) do
      @trend = FactoryGirl.create(:trend)
    end

    context "with no from_date or to_date" do
      before(:each) do
        get :show, id: @trend.id, :format => :json
      end

      it "responds with success" do
        expect(response).to be_success
      end

      it "responds with information for one trend" do
        expect(json).to have_key('name')
      end

      it "responds with no data_points" do
        expect(json['data_points'].length).to eq(0)
      end
    end

    context "with from_date and to_date" do
      before(:each) do
        get :show, id: @trend.id, from_date: "2013-07-08", to_date: "2015-03-12", :format => :json
      end

      it "responds with success" do
        expect(response).to be_success
      end

      it "responds with information for one trend" do
        expect(json).to have_key('name')
      end

      it "responds with data_points" do
        expect(json['data_points'].length).to be >= 0
      end
    end
  end

end
