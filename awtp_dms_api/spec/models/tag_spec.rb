require 'rails_helper'

RSpec.describe Tag, :type => :model do
  it "has a valid factory" do
    tag = FactoryGirl.build(:tag)
    expect(tag).to be_valid
  end

  it "is invalid without a name" do
    tag = FactoryGirl.build(:tag, name: nil)
    expect(tag).not_to be_valid
  end

  it "is invalid without a value" do
    tag = FactoryGirl.build(:tag, value: nil)
    expect(tag).not_to be_valid
  end
end
