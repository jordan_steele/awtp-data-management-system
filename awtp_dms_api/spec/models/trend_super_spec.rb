require 'rails_helper'

RSpec.describe TrendSuper, :type => :model do
  it "has a valid factory" do
    trend_ai011_pv = FactoryGirl.build(:trend_ai011_pv)
    expect(trend_ai011_pv).to be_valid
  end

  it "is invalid without a value" do
    trend_ai011_pv = FactoryGirl.build(:trend_ai011_pv, value: nil)
    expect(trend_ai011_pv).not_to be_valid
  end

  it "is invalid without a time" do
    trend_ai011_pv = FactoryGirl.build(:trend_ai011_pv, time: nil)
    expect(trend_ai011_pv).not_to be_valid
  end
end
