require 'faker'

FactoryGirl.define do 
  factory :digalm do |a|
    a.tag "Lxxxy_HAlarm"
    a.name { Faker::Lorem.word }
    a.desc "High Alarm"
    a.var_a "Lxxxy_HAlarm"
    a.var_b ""
    a.category "2"
    a.help ""
    a.priv ""
    a.area "1"
    a.comment ""
    # a.sequence ""
    a.delay ""
    a.custom1 ""
    a.custom2 ""
    a.custom3 ""
    a.custom4 ""
    a.custom5 ""
    a.custom6 ""
    a.custom7 ""
    a.custom8 ""
    a.cluster ""
    a.taggenlink ""
    a.paging ""
    a.paginggrp ""
  end
end
