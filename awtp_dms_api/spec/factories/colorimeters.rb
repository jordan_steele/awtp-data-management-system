# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :colorimeter do
    equipment_id 1
    date "2014-10-11 13:33:38"
    analyst "MyString"
    sol1 "MyString"
    sol2 "MyString"
    sol3 "500"
    comments "MyString"
  end
end
