# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :turbidity do
    Turbidity_Form_ID ""
    EquipmentID nil
    Formazin_Calibration_Date "2014-10-11 13:37:07"
    Formazin_Comments "MyString"
    Gelex_Check_Date "2014-10-11 13:37:07"
    Gelex_Comments "MyString"
    Other_Comments "MyString"
  end
end
