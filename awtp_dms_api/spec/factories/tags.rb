# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :tag do |f|
    f.name "MyString"
    f.value 1
  end
end
