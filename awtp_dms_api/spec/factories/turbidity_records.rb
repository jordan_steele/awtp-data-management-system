# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :turbidity_record do
    Turbidity_Record_ID ""
    Turbidity_Form_ID nil
    Type ""
    Calibration_Check 1
    Range_Reading 1.5
    Tick false
  end
end
