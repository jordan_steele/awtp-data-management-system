# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :p_h_calibration do
    pH_Record_ID ""
    EquipmentID nil
    Date "2014-10-11 13:27:56"
    Analyst "MyString"
    Change_Of_Buffers false
    Check_or_Calibration "MyString"
    BufferCalibration4 "MyString"
    BufferCalibration7 "MyString"
    BufferCalibration10 "MyString"
    Slope "MyString"
    Comments "MyString"
  end
end
