# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :user do |u|
    u.email { Faker::Internet.email }
    u.password { Faker::Lorem.characters(10) }
    u.password_confirmation { "#{password}" }
    u.reset_password_token { Faker::Lorem.characters(32) }
    u.reset_password_sent_at { Date.today - Faker::Number.number(3).to_i.days }
    u.remember_created_at { Date.today - Faker::Number.number(3).to_i.days }
    u.sign_in_count Faker::Number.number(3)
    u.current_sign_in_at { Date.today - Faker::Number.number(3).to_i.days }
    u.last_sign_in_at { Date.today - Faker::Number.number(3).to_i.days }
    u.current_sign_in_ip { Faker::Internet.ip_v4_address }
    u.last_sign_in_ip { Faker::Internet.ip_v4_address }
    u.authentication_token { Faker::Lorem.characters(32) }
    u.created_at { Date.today - Faker::Number.number(3).to_i.days }
    u.updated_at { Date.today - Faker::Number.number(3).to_i.days }
    u.admin false
  end
end
