# Read about factories at https://github.com/thoughtbot/factory_girl
require 'faker'

FactoryGirl.define do
  factory :trend do |t|
    t.name "Lxxxy_PV"
    t.expr { Faker::Lorem.word }
    t.trig ""
    t.sampleper "00:00:05"
    t.priv ""
    t.area ""
    t.eng_units "L/min"
    t.format "##.#EU"
    t.filename "[DATA]:Lxxxy_PV"
    t.files { Faker::Number.number(3) }
    t.time "00:00:00"
    t.period "Sunday"
    t.comment { Faker::Lorem.word }
    t.trendtype { Faker::Lorem.word }
    t.spcflag ""
    t.lsl ""
    t.usl ""
    t.subgrpsize ""
    t.xdoublebar ""
    t.range ""
    t.sdeviation ""
    t.stormethod "Floating Point (8-byte samples)"
    t.cluster ""
    t.taggenlink ""
  end
end
