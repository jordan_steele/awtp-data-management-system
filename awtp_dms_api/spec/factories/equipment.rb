# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :equipment do
    location "MyString"
    model "MyString"
    equipment_number "MyString"
    serial_number "MyString"
    make "MyString"
  end
end
