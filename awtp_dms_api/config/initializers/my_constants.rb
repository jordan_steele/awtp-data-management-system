MAX_DATA_POINTS = 1000

### API Examples

# Trends
TREND_EXAMPLE_SHOW = " 'trend': {
                id: 1
                name: \"Lxxxy_PV\", 
                expr: \"Lxxxy_PV\",
                trig: \"\",
                sampleper: \"00:00:05\",
                priv: \"\",
                area: \"\",
                eng_units: \"L/min\",
                format: \"##.#EU\",
                filename: \"[DATA]:Lxxxy_PV\",
                files: \"104\",
                time: \"00:00:00\",
                period: \"Sunday\",
                comment: \"Inlet - pH\",
                trendtype: \"TRN_PERIODIC\",
                spcflag: \"\",
                lsl: \"\",
                usl: \"\",
                subgrpsize: \"\",
                xdoublebar: \"\",
                range: \"\",
                sdeviation: \"\",
                stormethod: \"Floating Point (8-byte samples)\",
                cluster: \"\",
                taggenlink: \"\",
                data_points: \"[{value: 1.29, time: '2014-09-12T09:32:00.555Z' }...{value: 3.2, time: '2014-09-12T09:32:37.626Z }]\"
          } "

TREND_EXAMPLE_INDEX = "[ {
                id: 1
                name: \"Lxxxy_PV\", 
                expr: \"Lxxxy_PV\",
                trig: \"\",
                sampleper: \"00:00:05\",
                priv: \"\",
                area: \"\",
                eng_units: \"L/min\",
                format: \"##.#EU\",
                filename: \"[DATA]:Lxxxy_PV\",
                files: \"104\",
                time: \"00:00:00\",
                period: \"Sunday\",
                comment: \"Inlet - pH\",
                trendtype: \"TRN_PERIODIC\",
                spcflag: \"\",
                lsl: \"\",
                usl: \"\",
                subgrpsize: \"\",
                xdoublebar: \"\",
                range: \"\",
                sdeviation: \"\",
                stormethod: \"Floating Point (8-byte samples)\",
                cluster: \"\",
                taggenlink: \"\"
} ,
... 
]"

# Alarms
ALARM_EXAMPLE_SHOW = " 'alarm': {
                id: 20,
                tag: \"L3030_OLoadFlt\",
                name: \"@(L3030)\",
                desc: \"Overload Fault\",
                var_a: \"L3030_OLoadFlt\",
                var_b: \"\",
                category: 2,
                help: \"\",
                priv: \"\",
                area: \"1\",
                comment: \"@(L3030)\",
                sequence: \"\",
                delay: \"\",
                custom1: \"\",
                custom2: \"\",
                custom3: \"\",
                custom4: \"\",
                custom5: \"\",
                custom6: \"\",
                custom7: \"\",
                custom8: \"\",
                cluster: \"\",
                taggenlink: \"\",
                paging: \"\",
                paginggrp: \"\"
          } "

ALARM_EXAMPLE_INDEX = "[ {
                id: 20,
                tag: \"L3030_OLoadFlt\",
                name: \"@(L3030)\",
                desc: \"Overload Fault\",
                var_a: \"L3030_OLoadFlt\",
                var_b: \"\",
                category: 2,
                help: \"\",
                priv: \"\",
                area: \"1\",
                comment: \"@(L3030)\",
                sequence: \"\",
                delay: \"\",
                custom1: \"\",
                custom2: \"\",
                custom3: \"\",
                custom4: \"\",
                custom5: \"\",
                custom6: \"\",
                custom7: \"\",
                custom8: \"\",
                cluster: \"\",
                taggenlink: \"\",
                paging: \"\",
                paginggrp: \"\"
} ,
... 
]"

# SESSIONS
SESSIONS_CREATE_SUCCESS_EXAMPLE = "{
        \"info\":\"Logged in\",
        \"user\":{
                \"id\":2,
                \"email\":\"user2@example.com\",
                \"authentication_token\":\"pLf9b6S7d52k3ryB7gLG\"
        }
}"

SESSIONS_CREATE_FAILURE_EXAMPLE = "{
        \"error\":\"Invalid email address or password.\"
}"

SESSIONS_DESTROY_SUCCESS_EXAMPLE = "{
        \"info\":\"Logged out\"
}"

SESSIONS_DESTROY_FAILURE_EXAMPLE = "{
        \"error\":\"Invalid email address or password.\"
}"

# USERS
USERS_INDEX_EXAMPLE = "[ {
        \"id\": 2,
        \"email\": \"user@example.com\",
        \"created_at\": \"2014-07-28T00:00:00.000Z\",
        \"admin\": false
},
...
]"

USERS_EXAMPLE = "{
        \"id\": 2,
        \"email\": \"user@example.com\",
        \"created_at\": \"2014-07-28T00:00:00.000Z\",
        \"admin\": false
}"

# COLORIMETERS
COLORIMETERS_INDEX_EXAMPLE = "[ {
        \"equipment_id\": 1,
        \"Date\": \"2014-07-28T00:00:00.000Z\",
        \"Analyst\": \"Jason\",
        \"Type\": \"Pt/Co\",
        \"Sol1\": \"500\",
        \"Sol2\": \"475-525\",
        \"Sol3\": \"498\",
        \"Comments\": \"Pass, next Monday\",
        \"created_at\": \"2014-07-28T00:00:00.000Z\",
        \"updated_at\": \"2014-07-28T00:00:00.000Z\"
},
...
]"

COLORIMETERS_EXAMPLE = "{
        \"equipment_id\": 1,
        \"Date\": \"2014-07-28T00:00:00.000Z\",
        \"Analyst\": \"Jason\",
        \"Type\": \"Pt/Co\",
        \"Sol1\": \"500\",
        \"Sol2\": \"475-525\",
        \"Sol3\": \"498\",
        \"Comments\": \"Pass, next Monday\",
        \"created_at\": \"2014-07-28T00:00:00.000Z\",
        \"updated_at\": \"2014-07-28T00:00:00.000Z\"
}"

# PHCALIBRATIONS
PHCALIBRATION_INDEX_EXAMPLE = "[ {
        \"equipment_id\": 1,
        \"Date\": \"2014-07-28T00:00:00.000Z\",
        \"Analyst\": \"Jason\",
        \"Change_Of_Buffers\": true,
        \"Check_or_Calibration\": \"CK\",
        \"BufferCalibration4\": \"Yes\",
        \"BufferCalibration7\": \"No\",
        \"BufferCalibration10\": \"No\",
        \"Slope\": \"38\",
        \"Comments\": \"This is awesome\",
        \"created_at\": \"2014-07-28T00:00:00.000Z\",
        \"updated_at\": \"2014-07-28T00:00:00.000Z\",
},
...
]"

PHCALIBRATION_EXAMPLE = "{
        \"equipment_id\": 1,
        \"Date\": \"2014-07-28T00:00:00.000Z\",
        \"Analyst\": \"Jason\",
        \"Change_Of_Buffers\": true,
        \"Check_or_Calibration\": \"CK\",
        \"BufferCalibration4\": \"Yes\",
        \"BufferCalibration7\": \"No\",
        \"BufferCalibration10\": \"No\",
        \"Slope\": \"38\",
        \"Comments\": \"This is awesome\",
        \"created_at\": \"2014-07-28T00:00:00.000Z\",
        \"updated_at\": \"2014-07-28T00:00:00.000Z\",
}"
