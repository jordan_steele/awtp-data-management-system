Rails.application.routes.draw do 

  devise_for :users
  apipie
  
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      # needed for CORS non-GET requests
      match "/tags" => "tags#index", via: :options
      
      resources :tags, only: [:index, :show, :create, :update, :destroy]

      resources :trends, only: [:index, :show]
      
      resources :alarms, only: [:index, :show]

      resources :users, only: [:index, :show, :create, :update, :destroy]

      resources :operator_forms, only: [:index, :show, :create, :update, :destroy]

      resources :equipments, only: [:index, :show, :create, :update, :destroy]

      resources :colorimeters, only: [:index, :show, :create, :update, :destroy]

      resources :ph_calibrations, only: [:index, :show, :create, :update, :destroy]

      devise_scope :user do
        # match '/sessions' => 'sessions#create', :via => :options
        match '/sessions' => 'sessions#create', :via => :post
        match '/sessions' => 'sessions#destroy', :via => :delete
      end
    end
  end
end
