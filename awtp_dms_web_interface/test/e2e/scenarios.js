'use strict';

/* https://github.com/angular/protractor/blob/master/docs/getting-started.md */

describe('awtpApp', function() {

  browser.get('index-e2e.html');
  browser.manage().window().setSize(1080, 1920); // some elements can't be found

  it('should automatically redirect to /tags when location is empty', function() {
    expect(browser.getLocationAbsUrl()).toMatch("/tags");
  });


  describe('Tags view', function() {

    beforeEach(function() {
      browser.get('index-e2e.html#/tags');
    });

    it('should filter the tag list as the user types into the search box', function() {
      var tagList = element.all(by.repeater('tag in tags'));
      var searchBox = element(by.model('query'));

      expect(tagList.count()).toBe(11);

      searchBox.sendKeys('tag0003');
      expect(tagList.count()).toBe(1);

      searchBox.clear();
      searchBox.sendKeys('tag001');
      expect(tagList.count()).toBe(2);
    });

    it('should render tag specific links', function() {
      var searchBox = element(by.model('query'));
      searchBox.sendKeys('tag0003');
      element(by.css('.list-group-item')).click();
      browser.getLocationAbsUrl().then(function(url) {
        expect(url.split('#')[1]).toBe('/tags/3');
      });
    });

  });

  describe('Tag detail view', function() {

    beforeEach(function() {
      browser.get('index-e2e.html#/tags/1');
    });

    it('should display tag0001 page', function() {
      expect(element(by.binding('tag.name')).getText()).toBe('tag0001');
    });
  });

});