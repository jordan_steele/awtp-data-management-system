'use strict';

var awtpAppTest = angular.module('awtpAppTest', ['awtpApp', 'ngMockE2E']);
  
awtpAppTest.run(function($httpBackend, HOST, TAGS_END_POINT) {
  var tags = [
    { id: 1, name: 'tag0001', value:10},
    { id: 2, name: 'tag0002', value:20},
    { id: 3, name: 'tag0003', value:22},
    { id: 4, name: 'tag0004', value:45},
    { id: 5, name: 'tag0005', value:63},
    { id: 6, name: 'tag0006', value:113},
    { id: 7, name: 'tag0007', value:1},
    { id: 8, name: 'tag0008', value:14},
    { id: 9, name: 'tag0009', value:12},
    { id: 10, name: 'tag0010', value:12},
    { id: 11, name: 'tag0011', value:53}
  ];

  // returns the current list of tags
  $httpBackend.whenGET(HOST + TAGS_END_POINT).respond(tags);

  // returns a single tag
  var regex = new RegExp(HOST + TAGS_END_POINT + '/([0-9]+)');
  $httpBackend.whenGET(regex).respond(
    function(method, url) {
      var id = url.match(regex)[1];
      var lookup = {};
      for (var i = 0, len = tags.length; i < len; i++) {
          lookup[tags[i].id] = tags[i];
      }
      return [200, lookup[id], {}];
    });

  // $httpBackend.whenGET(/^\/templates\//).passThrough();
  $httpBackend.whenGET(/.*/).passThrough();
});