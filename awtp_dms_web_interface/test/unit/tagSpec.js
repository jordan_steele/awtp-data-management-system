describe('Tag controllers', function () {
  var scope, ctrl, $httpBackend;

  beforeEach(function(){
    this.addMatchers({
      toEqualData: function(expected) {
        return angular.equals(this.actual, expected);
      }
    });
  });

  beforeEach(module('awtpApp'));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });


  describe('TagListCtrl', function() {
    var tagListData = function() {
      return [
        {id: 1, name: 'tag0001', value:10}, 
        {id: 2, name: 'tag0002', value:30}
      ]
    };

    beforeEach(inject(function(_$httpBackend_, $rootScope, $controller, HOST, TAGS_END_POINT) {
        $httpBackend = _$httpBackend_;
        $httpBackend.expectGET(HOST + TAGS_END_POINT).respond(tagListData());
        scope = $rootScope.$new();
        ctrl = $controller('TagListCtrl', {$scope: scope});
    }));

    it('should create "tags" model with 2 tags fetched from server', function() {
      expect(scope.tags).toEqualData([]);
      $httpBackend.flush();

      expect(scope.tags).toEqualData(tagListData());
    });
  });


  describe('TagDetailCtrl', function() {
    var tagDetailData = function() {
      return {id: 1, name: 'tag0001', value: 10}
    };

    beforeEach(inject(function(_$httpBackend_, $rootScope, $stateParams, $controller, HOST, TAGS_END_POINT) {
        $httpBackend = _$httpBackend_;
        $httpBackend.expectGET(HOST + TAGS_END_POINT + '/1').respond(tagDetailData());
        $stateParams.tagId = 1;
        scope = $rootScope.$new();
        ctrl = $controller('TagDetailCtrl', {$scope: scope});
    }));

    it('should fetch tag detail', function() {
      expect(scope.tag).toEqualData({});
      $httpBackend.flush();

      expect(scope.tag).toEqualData(tagDetailData());
    });
  });

});