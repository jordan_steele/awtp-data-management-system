'use strict';

var colorimeterService = angular.module('colorimeterService', ['ngResource', 'awtpConfiguration']);

colorimeterService.factory('Colorimeter', ['$resource', 'HOST', 'COLORIMETER_END_POINT',
  function($resource, HOST, COLORIMETER_END_POINT){
    return $resource(HOST + COLORIMETER_END_POINT + '/:id', {}, {
    });
  }]);
