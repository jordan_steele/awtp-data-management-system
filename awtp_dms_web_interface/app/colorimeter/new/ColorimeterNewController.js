'use strict';

var colorimeterNewController = angular.module('colorimeterNewController', []);

colorimeterNewController.controller('ColorimeterNewCtrl', ['$rootScope', '$scope', '$state', '$stateParams', '$alert', 'Colorimeter', 
  function ($rootScope, $scope, $state, $stateParams, $alert, Colorimeter) {
    $scope.errors = {};

    $scope.save = function() {
      $scope.colorimeter.equipment_id = $stateParams.equipmentId;

      $scope.newColorimeterForm['date'].$setValidity('server', true);
      $scope.newColorimeterForm['analyst'].$setValidity('server', true);
      $scope.newColorimeterForm['sol1'].$setValidity('server', true);
      $scope.newColorimeterForm['sol2'].$setValidity('server', true);
      $scope.newColorimeterForm['sol3'].$setValidity('server', true);
      $scope.newColorimeterForm['comments'].$setValidity('server', true);

      $scope.$broadcast('show-errors-check-validity');
      
      if ($scope.newColorimeterForm.$valid) {
        var colorimeter = new Colorimeter($scope.colorimeter);
        colorimeter.$save(colorimeter, 
          function(colorimeterResponse) {
            $rootScope.$broadcast('new-form-success');
          },
          function(errorResponse) {
            $scope.errors['form'] = errorResponse.error;
            $scope.newColorimeterForm['date'].$setValidity('server', false);
            $scope.newColorimeterForm['analyst'].$setValidity('server', false);
            $scope.newColorimeterForm['sol1'].$setValidity('server', false);
            $scope.newColorimeterForm['sol2'].$setValidity('server', false);
            $scope.newColorimeterForm['sol3'].$setValidity('server', false);
            $scope.newColorimeterForm['comments'].$setValidity('server', false);
          }
        );
      }
    };
  }]);