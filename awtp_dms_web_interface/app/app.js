'use strict';

// Declare app level module which depends on filters, and services
var awtpApp = angular.module('awtpApp', [
  'ngRoute',
  'ngCookies',
  // 'ngAnimate',
  'sessionService',
  'awtpConfiguration',
  'appController',
  'userService',
  'loginController',
  'registerController',
  'tagService',
  'tagDetailController',
  'tagListController',
  'tagNewController',
  'trendService',
  'trendDetailController',
  'trendListController',
  'equipmentService',
  'equipmentDetailController',
  'equipmentListController',
  'equipmentNewController',
  'colorimeterNewController',
  'colorimeterService',
  'phCalibrationNewController',
  'phCalibrationService',
  'userDetailController',
  'userListController',
  'awtpForm',
  'cssEqualHeight',
  'emptyIfBlankFilter',
  'toTrustedFilter',
  'mgcrea.ngStrap',
  'ui.router',
  'angles',
  'rzModule',
  'fui',
  'angularUtils.directives.dirPagination'
]);

awtpApp
  .run(['$rootScope', '$state', 'Session', 
    function($rootScope, $state, Session) {
      $rootScope.isState = function(state){
        return $state.includes(state);
      };

      Session.setHeaders();
    }])
  .config(['$alertProvider',
    function($alertProvider) {
      angular.extend($alertProvider.defaults, {
        placement: 'bottom',
        show: true,
        container: '#page-content-wrapper',
        duration: 3
      });
    }])
  .config(['$httpProvider',
    function($httpProvider){
      var interceptor = ['$location', '$rootScope', '$q', '$injector', 
        function($location, $rootScope, $q, $injector) {
          function success(response) {
            return response;
          }

          function error(response) {
            var Session = $injector.get('Session');
            if (response.status == 401) {
              $rootScope.$broadcast('event:unauthorized');
              Session.clearCurrentUser();
            }
            return $q.reject(response);
          }

          return function(promise) {
            return promise.then(success, error);
          };
        }];
      $httpProvider.responseInterceptors.push(interceptor);
    }])
  .config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise('/trends');

      $stateProvider.
        state('auth', {
          abstract: true,
          url: '',
          templateUrl: 'accounts/auth/auth.html'
        }).
        state('auth.login', {
          url: '/login',
          templateUrl: 'accounts/auth/login/login.html',
          controller: 'LoginCtrl'
        }).
        state('auth.register', {
          url:'/register',
          templateUrl: 'accounts/auth/register/register.html',
          controller: 'RegisterCtrl'
        }).

        state('app', {
          abstract: true,
          url: '',
          templateUrl: 'common/app.html',
          controller: 'AppCtrl'
        }).

        state('app.tags', {
          url: '/tags',
          views: {
            'sidebar': {
              templateUrl: 'tag/list/tag-list.html',
              controller: 'TagListCtrl'
            }
          }
        }).
        state('app.tags.detail', {
          url: '/{tagId:[0-9]{1,8}}',
          views: {
            "main@app": {
              templateUrl:'tag/detail/tag-detail.html',
              controller: 'TagDetailCtrl'
            }
          }
        }).
        state('app.tags.new', {
          url: '/new',
          views: {
            "main@app": {
              templateUrl: 'tag/new/tag-new.html',
              controller: 'TagNewCtrl'
            }
          }
        }).

        state('app.trends', {     // TRENDS //
          url: '/trends',
          views: {
            'sidebar': {
              templateUrl: 'trend/list/trend-list.html',
              controller: 'TrendListCtrl'
            }
          }
        }).
        state('app.trends.detail', {
          url: '/{trendId:[0-9]{1,8}}',
          views: {
            "main@app": {
              templateUrl:'trend/detail/trend-detail.html',
              controller: 'TrendDetailCtrl'
            }
          }
        }).

        state('app.users', {     // USERS //
          url: '/users',
          views: {
            'sidebar': {
              templateUrl: 'user/list/user-list.html',
              controller: 'UserListCtrl'
            }
          }
        }).
        state('app.users.detail', {
          url: '/{userId:[0-9]{1,8}}',
          views: {
            "main@app": {
              templateUrl:'user/detail/user-detail.html',
              controller: 'UserDetailCtrl'
            }
          }
        }).

        state('app.equipments', {     // EQUIPMENT //
          url: '/equipment',
          views: {
            'sidebar': {
              templateUrl: 'equipment/list/equipment-list.html',
              controller: 'EquipmentListCtrl'
            }
          }
        }).
        state('app.equipments.detail', {
          url: '/{equipmentId:[0-9]{1,8}}',
          views: {
            "main@app": {
              templateUrl:'equipment/detail/equipment-detail.html',
              controller: 'EquipmentDetailCtrl'
            }
          }
        }).
        state('app.equipments.detail.newColorimeter', {
          url: '',
          templateUrl: 'colorimeter/new/colorimeter-new.html',
          controller: 'ColorimeterNewCtrl'
        }).
        state('app.equipments.new', {
          url: '/new',
          views: {
            "main@app": {
              templateUrl: 'equipment/new/equipment-new.html',
              controller: 'EquipmentNewCtrl'
            }
          }
        });
    }]);           
