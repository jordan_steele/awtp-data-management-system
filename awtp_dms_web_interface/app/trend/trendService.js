'use strict';

var trendService = angular.module('trendService', ['ngResource', 'awtpConfiguration']);

trendService.factory('Trend', ['$resource', 'HOST', 'TRENDS_END_POINT',
  function($resource, HOST, TRENDS_END_POINT){
    return $resource(HOST + TRENDS_END_POINT + '/:id', {}, {
    });
  }]);
