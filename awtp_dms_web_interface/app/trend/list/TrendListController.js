'use strict';

var trendListController = angular.module('trendListController', []);

trendListController.controller('TrendListCtrl', ['$scope', '$location', 'Trend', 
  function ($scope, $location, Trend) {
    $scope.trends = Trend.query();
    var currentTrendSetByClick = false;

    if ($scope.$parent.currentTrend == null) {
      var currentTrendListener = $scope.$watch (
        function() {
          return $scope.$parent.currentTrend;
        },
        function(newValue, oldValue) {
          if (newValue != oldValue) {
            if (!currentTrendSetByClick) {
              $scope.query = $scope.$parent.currentTrend.name;
            }
            currentTrendListener();
          }
        })
    }

    $scope.updateCurrentTrend = function(trend) {
      $scope.$parent.currentTrend = trend;
      currentTrendSetByClick = true;
    }
  }]);