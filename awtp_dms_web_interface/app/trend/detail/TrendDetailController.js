'use strict';

var trendDetailController = angular.module('trendDetailController', []);

trendDetailController.controller('TrendDetailCtrl', ['$scope', '$stateParams', 'Trend',
  function($scope, $stateParams, Trend) {

    // Slider requires some initial values otherwise it will throw an error
    $scope.dateSlider = {
      min: 0,
      max: 100,
      ceil: 100,
      floor: 0
    };

    // Do the initial fetch of trend data
    $scope.trend = Trend.get({id: $stateParams.trendId}, function() {
      // Store the first and last dates on the scope for future use
      var one_day = 24*60*60*1000; // hours*minutes*seconds*milliseconds
      $scope.firstDate = new Date($scope.trend.data_points[0].time);
      $scope.secondDate = new Date($scope.trend.data_points[$scope.trend.data_points.length-1].time);

      // Calc the difference in days between the first and last date and update
      // the slider to reflect this
      var diffDays = Math.round(Math.abs(($scope.firstDate.getTime() - 
                      $scope.secondDate.getTime())/(one_day)));
      $scope.dateSlider = {
        min: 0,
        max: diffDays,
        ceil: diffDays,
        floor: 0
      };
      
      $scope.updateChart($scope.trend.data_points);

      if ($scope.$parent.currentTrend == null) {
        $scope.$parent.currentTrend = $scope.trend;
      }
    });

    // Watch the min and max slider value and update the chart data on change
    $scope.$watch(
      function() {
        return $scope.dateSlider.min
      }, 
      function(newValue, oldValue) {
        if (newValue != oldValue) {
          $scope.fetchDataAndUpdate();
        }
      }
    );
    $scope.$watch(
      function() {
        return $scope.dateSlider.max
      }, 
      function(newValue, oldValue) {
        if (newValue != oldValue) {
          $scope.fetchDataAndUpdate();
        }
      }
    );

    $scope.fetchDataAndUpdate = function() {
      var from_date = new Date();
      var to_date = new Date();
      from_date.setDate($scope.firstDate.getDate() + $scope.dateSlider.min);
      to_date.setDate($scope.firstDate.getDate() + $scope.dateSlider.max);

      $scope.trend = Trend.get({id: $stateParams.trendId,
        from_date: $scope.dateToString(from_date), 
        to_date: $scope.dateToString(to_date)}, 
          function() {
            if ($scope.trend.data_points) {
              $scope.updateChart($scope.trend.data_points)
            }
          }
      );
    }

    $scope.updateChart = function(dataPoints) {
      var chartData = $scope.calcChartData(dataPoints);

      $scope.chart1 = {
        labels: chartData.labels,
        datasets : [
          {
            fillColor : "rgba(151,187,205,0)",
            strokeColor : "#3498db",
            pointColor : "rgba(151,187,205,0)",
            pointStrokeColor : "#3498db",
            data : chartData.data
          }
        ]
      };
    }

    $scope.calcChartData = function(dataPoints) {
      var labels = [];
      var data = [];
      var labelInterval = Math.ceil(dataPoints.length / 5);

      for (var i = 0; i < dataPoints.length; i++) {
        // hacky solution, chartjs currently requires an x label for every data point
        // workaround is just to set every n labels correctly and the rest to a blank string
        if (i == dataPoints.length-1) {
          labels[i] = new Date(dataPoints[i].time).toLocaleDateString();
        } else {
          if (i % labelInterval == 0) {
            labels[i] = new Date(dataPoints[i].time).toLocaleDateString();
          } else {
            labels[i] = ""
          }
        }

        data[i] = dataPoints[i].value;
      }

      return {
        labels: labels,
        data: data
      };
    }

    $scope.dateToString = function(date) {
      var curr_date = date.getDate();
      var curr_month = date.getMonth() + 1;
      var curr_year = date.getFullYear();
      return curr_year + "-" + curr_month + "-" + curr_date;
    }

    // Function to translate an int representing the days from the first date in
    // the trend data to a date string for display to user
    $scope.translateDaysFromStartToDate = function(value)
    {
      if (!$scope.firstDate) {
        return 0
      };
      return new Date($scope.firstDate.getTime()+value*24*60*60*1000).toLocaleDateString();
    }

    $scope.chartOptions = {
      animation: false,
      scaleShowGridLines: false,
      showTooltips: false,
      responsive: true,
      pointDot : false
    };

  }]);