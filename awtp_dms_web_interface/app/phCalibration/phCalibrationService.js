'use strict';

var phCalibrationService = angular.module('phCalibrationService', ['ngResource', 'awtpConfiguration']);

phCalibrationService.factory('PhCalibration', ['$resource', 'HOST', 'PHCALIBRATION_END_POINT',
  function($resource, HOST, PHCALIBRATION_END_POINT){
    return $resource(HOST + PHCALIBRATION_END_POINT + '/:id', {}, {
    });
  }]);
