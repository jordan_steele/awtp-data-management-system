'use strict';

var phCalibrationNewController = angular.module('phCalibrationNewController', []);

phCalibrationNewController.controller('pHCalibrationNewCtrl', ['$rootScope', '$scope', '$state', '$stateParams', '$alert', 'PhCalibration', 
  function ($rootScope, $scope, $state, $stateParams, $alert, PhCalibration) {
    $scope.errors = {};

    $scope.save = function() {
      $scope.phCalibration.equipment_id = $stateParams.equipmentId;

      $scope.newPhCalibrationForm['date'].$setValidity('server', true);
      $scope.newPhCalibrationForm['analyst'].$setValidity('server', true);
      $scope.newPhCalibrationForm['change_of_buffers'].$setValidity('server', true);
      $scope.newPhCalibrationForm['check_or_calibration'].$setValidity('server', true);
      $scope.newPhCalibrationForm['buffer_calibration'].$setValidity('server', true);
      $scope.newPhCalibrationForm['slope'].$setValidity('server', true);
      $scope.newPhCalibrationForm['comments'].$setValidity('server', true);

      $scope.$broadcast('show-errors-check-validity');
      
      if ($scope.newPhCalibrationForm.$valid) {
        var phCalibration = new PhCalibration($scope.phCalibration);
        phCalibration.$save(phCalibration, 
          function(phCalibrationResponse) {
            $rootScope.$broadcast('new-form-success');
          },
          function(errorResponse) {
            $scope.errors['form'] = errorResponse.error;
            $scope.newPhCalibrationForm['date'].$setValidity('server', false);
            $scope.newPhCalibrationForm['analyst'].$setValidity('server', false);
            $scope.newPhCalibrationForm['change_of_buffers'].$setValidity('server', false);
            $scope.newPhCalibrationForm['check_or_calibration'].$setValidity('server', false);
            $scope.newPhCalibrationForm['buffer_calibration'].$setValidity('server', false);
            $scope.newPhCalibrationForm['slope'].$setValidity('server', false);
            $scope.newPhCalibrationForm['comments'].$setValidity('server', false);
          }
        );
      }
    };
  }]);