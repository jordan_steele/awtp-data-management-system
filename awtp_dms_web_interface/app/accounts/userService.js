'use strict';

var userService = angular.module('userService', ['ngResource']);

userService.factory('User', ['$resource', 'HOST', 'USERS_END_POINT',
  function($resource, HOST, USERS_END_POINT){
    return $resource(HOST + USERS_END_POINT + '/:id', {}, {
      'update': { method:'PUT' }
    });
  }]);