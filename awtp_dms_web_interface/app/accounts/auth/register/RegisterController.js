'use strict';

var registerController = angular.module('registerController', []);

registerController.controller('RegisterCtrl', ['$scope', '$alert', '$state', 'Session', 'User',
  function ($scope, $alert, $state, Session, User) {
    if (Session.requestCurrentUser()) {
      $state.go('app.tags');
    }

    $scope.register = function() {
      $scope.$broadcast('show-errors-check-validity');

      if ($scope.registerForm.$valid) {
        var user = new User($scope.user);
        user.$save(user, 
          function(userResponse) {
            // On success change path to the new tag
            $state.go('auth.login');
          },
          function(userResponse) {
            // On failure, display errors
            $scope.errors = {}
            angular.forEach(userResponse.data.error, function(error, field) {
              $scope.registerForm[field].$setValidity('server', false);
              $scope.errors[field] = error.join(', ');
            });
          }
        );
      }
    };
  }]);