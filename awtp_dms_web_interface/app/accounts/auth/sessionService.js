'use strict';

var sessionService = angular.module('sessionService', ['awtpConfiguration']);

sessionService.factory('Session', ['$location', '$http', '$q', '$cookieStore', 'HOST', 'SESSIONS_END_POINT',
  function($location, $http, $q, $cookieStore, HOST, SESSIONS_END_POINT) {
    function redirect(url) {
      url = url || '/trends';
      $location.path(url);
    }
    var service = {
      login: function(email, password) {
        return $http.post(HOST+SESSIONS_END_POINT, {user: {email: email, password: password} })
        .success(function(response) {
          $cookieStore.put('currentUser', response.user);
          service.setHeaders();
          redirect();
        });
      },

      logout: function() {
        $http.delete(HOST+SESSIONS_END_POINT).then(function(response) {
          service.clearCurrentUser();
        });
      },

      requestCurrentUser: function() {
        if (service.isAuthenticated()) {
          return $cookieStore.get('currentUser');
        } else {
          null;
        }
      },

      clearCurrentUser: function() {
        $cookieStore.remove('currentUser');
        service.setHeaders();
        redirect('/login');
      },

      setHeaders: function() {
        var currentUser = service.requestCurrentUser();
        if (currentUser) {
          $http.defaults.headers.common['X-User-Email'] = currentUser.email;
          $http.defaults.headers.common['X-User-Token'] = currentUser.authentication_token;
        } else {
          $http.defaults.headers.common['X-User-Email'] = null;
          $http.defaults.headers.common['X-User-Token'] = null;
        }
      },

      isAuthenticated: function(){
        return !!$cookieStore.get('currentUser');
      }
    };
    return service;
  }]);
