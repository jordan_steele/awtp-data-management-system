'use strict';

var loginController = angular.module('loginController', []);

loginController.controller('LoginCtrl', ['$scope', '$alert', '$state', 'Session',
  function ($scope, $alert, $state, Session) {
    $scope.errors = {};

    if (Session.requestCurrentUser()) {
      $state.go('app.trends');
    }

    $scope.login = function() {
      $scope.loginForm['email'].$setValidity('server', true);
      $scope.loginForm['password'].$setValidity('server', true);

      $scope.$broadcast('show-errors-check-validity');

      if ($scope.loginForm.$valid) {
        Session.login($scope.login.email, $scope.login.password)
          .error(function(response) {
            $scope.errors['form'] = response.error;
            $scope.loginForm['email'].$setValidity('server', false);
            $scope.loginForm['password'].$setValidity('server', false);
          });
      }
    };
  }]);