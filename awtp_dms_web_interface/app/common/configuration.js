'use strict';

var configuration = angular.module('awtpConfiguration', []);

configuration
  .constant('HOST', 'http://localhost:3000/api/v1')
  .constant('TAGS_END_POINT', '/tags')
  .constant('TRENDS_END_POINT', '/trends')
  .constant('SESSIONS_END_POINT', '/sessions')
  .constant('USERS_END_POINT', '/users')
  .constant('EQUIPMENT_END_POINT', '/equipments')
  .constant('COLORIMETER_END_POINT', '/colorimeters')
  .constant('PHCALIBRATION_END_POINT', '/ph_calibrations');