var emptyIfBlankFilter = angular.module("emptyIfBlankFilter", []);

emptyIfBlankFilter.filter("emptyifblank", function(){ return function(object, query){
  if(!query)
    return {};
  else
    return object;
}});
