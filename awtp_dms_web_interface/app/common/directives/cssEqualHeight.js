'use strict';

var cssEqualHeight = angular.module('cssEqualHeight', []);

cssEqualHeight.directive('cssEqualHeight', function () {
  return {
    restrict: 'A',
    scope: {
    },
    link: function (scope, element, attr) {
      var elementToCopyProperty = attr.cssEqualHeight,
          dest = document.getElementById(elementToCopyProperty.toString());
      scope.$watch(
        function () {
          return element.height();
        },
        function (newValue, oldValue) {
          if (newValue != oldValue) {
            dest.style.height = element[0].clientHeight + 'px';
          }
        }
      );
    }
  };
})