'use strict';

var appController = angular.module('appController', []);

appController.controller('AppCtrl', ['$scope', 'Session',
  function ($scope, Session) {
    $scope.currentUser = Session.requestCurrentUser();
    // if (currentUser) {
    //   $scope.currentUserEmail = currentUser.email;
    // } else {
    //   $scope.currentUserEmail = null
    // }

    $scope.logout = function() {
      Session.logout();
    };
  }]);