'use strict';

var tagDetailController = angular.module('tagDetailController', []);

tagDetailController.controller('TagDetailCtrl', ['$scope', '$stateParams', 'Tag',
  function($scope, $stateParams, Tag) {
    $scope.tag = Tag.get({id: $stateParams.tagId});
    $scope.chart1 = {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets : [
        {
          fillColor : "rgba(151,187,205,0)",
          strokeColor : "#f1c40f",
          pointColor : "rgba(151,187,205,0)",
          pointStrokeColor : "#f1c40f",
          data : [0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4]
        },
        {
          fillColor : "rgba(151,187,205,0)",
          strokeColor : "#e74c3c",
          pointColor : "rgba(151,187,205,0)",
          pointStrokeColor : "#e74c3c",
          data : [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2]
        },
        {
          fillColor : "rgba(151,187,205,0)",
          strokeColor : "#f1c40f",
          pointColor : "rgba(151,187,205,0)",
          pointStrokeColor : "#f1c40f",
          data : [0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8]
        },
        {
          fillColor : "rgba(151,187,205,0)",
          strokeColor : "#e74c3c",
          pointColor : "rgba(151,187,205,0)",
          pointStrokeColor : "#e74c3c",
          data : [1, 1, 1, 1, 1, 1, 1]
        },
        {
          fillColor : "rgba(151,187,205,0)",
          strokeColor : "#3498db",
          pointColor : "rgba(151,187,205,0)",
          pointStrokeColor : "#3498db",
          data : [0.43, 0.65, 0.93, 0.75, 0.63, 0.24, 0.47]
        }

      ]
    };
    $scope.chart2 = {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets : [
        {
          fillColor : "rgba(151,187,205,0)",
          strokeColor : "#f1c40f",
          pointColor : "rgba(151,187,205,0)",
          pointStrokeColor : "#f1c40f",
          data : [0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4]
        },
        {
          fillColor : "rgba(151,187,205,0)",
          strokeColor : "#e74c3c",
          pointColor : "rgba(151,187,205,0)",
          pointStrokeColor : "#e74c3c",
          data : [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2]
        },
        {
          fillColor : "rgba(151,187,205,0)",
          strokeColor : "#f1c40f",
          pointColor : "rgba(151,187,205,0)",
          pointStrokeColor : "#f1c40f",
          data : [0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8]
        },
        {
          fillColor : "rgba(151,187,205,0)",
          strokeColor : "#e74c3c",
          pointColor : "rgba(151,187,205,0)",
          pointStrokeColor : "#e74c3c",
          data : [1, 1, 1, 1, 1, 1, 1]
        },
        {
          fillColor : "rgba(151,187,205,0)",
          strokeColor : "#3498db",
          pointColor : "rgba(151,187,205,0)",
          pointStrokeColor : "#3498db",
          data : [0.89, 0.73, 0.27, 0.42, 0.58, 0.43, 0.95]
        }

      ]
    };
    $scope.options = {
      responsive: true
    };
  }]);