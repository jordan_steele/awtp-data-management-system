'use strict';

var tagListController = angular.module('tagListController', []);

tagListController.controller('TagListCtrl', ['$scope', '$location', 'Tag', 
  function ($scope, $location, Tag) {
    $scope.tags = Tag.query();
  }]);