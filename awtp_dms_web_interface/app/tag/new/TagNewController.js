'use strict';

var tagNewController = angular.module('tagNewController', []);

tagNewController.controller('TagNewCtrl', ['$scope', '$location', '$alert', 'Tag', 
  function ($scope, $location, $alert, Tag) {
    $scope.save = function() {
      $scope.$broadcast('show-errors-check-validity');
      
      if ($scope.newTagForm.$valid) {
        var tag = new Tag($scope.tag);
        tag.$save(tag, function(tagResponse) {

          // On success change path to the new tag
          $location.path('/tags/' + tagResponse.id);
        });
      }
    };

    $scope.reset = function() {
      $scope.$broadcast('show-errors-reset');
      $scope.tag = { name: '', value: '' };
    }
  }]);