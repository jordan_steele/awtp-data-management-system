'use strict';

var tagService = angular.module('tagService', ['ngResource']);

tagService.factory('Tag', ['$resource', 'HOST', 'TAGS_END_POINT',
  function($resource, HOST, TAGS_END_POINT){
    return $resource(HOST + TAGS_END_POINT + '/:id', {}, {
    });
  }]);
