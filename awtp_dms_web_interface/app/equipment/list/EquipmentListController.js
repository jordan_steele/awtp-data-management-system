'use strict';

var equipmentListController = angular.module('equipmentListController', []);

equipmentListController.controller('EquipmentListCtrl', ['$scope', '$location', 'Equipment', 
  function ($scope, $location, Equipment) {
    $scope.equipments = Equipment.query();
    var currentEquipmentSetByClick = false;

    if ($scope.$parent.currentEquipment == null) {
      var currentEquipmentListener = $scope.$watch (
        function() {
          return $scope.$parent.currentEquipment;
        },
        function(newValue, oldValue) {
          if (newValue != oldValue) {
            if (!currentEquipmentSetByClick) {
              $scope.query = $scope.$parent.currentEquipment.model;
            }
            currentEquipmentListener();
          }
        })
    }

    $scope.updateCurrentEquipment = function(equipment) {
      $scope.$parent.currentEquipment = equipment;
      currentEquipmentSetByClick = true;
    }
  }]);