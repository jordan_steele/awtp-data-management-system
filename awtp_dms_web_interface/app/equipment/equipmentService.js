'use strict';

var equipmentService = angular.module('equipmentService', ['ngResource', 'awtpConfiguration']);

equipmentService.factory('Equipment', ['$resource', 'HOST', 'EQUIPMENT_END_POINT',
  function($resource, HOST, EQUIPMENT_END_POINT){
    return $resource(HOST + EQUIPMENT_END_POINT + '/:id', {}, {
    });
  }]);
