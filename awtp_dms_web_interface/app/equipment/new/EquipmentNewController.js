'use strict';

var equipmentNewController = angular.module('equipmentNewController', []);

equipmentNewController.controller('EquipmentNewCtrl', ['$scope', '$state', '$alert', 'Equipment', 
  function ($scope, $state, $alert, Equipment) {
    $scope.errors = {};

    $scope.save = function() {
      $scope.newEquipmentForm['location'].$setValidity('server', true);
      $scope.newEquipmentForm['model'].$setValidity('server', true);
      $scope.newEquipmentForm['equipment_number'].$setValidity('server', true);
      $scope.newEquipmentForm['serial_number'].$setValidity('server', true);
      $scope.newEquipmentForm['make'].$setValidity('server', true);

      $scope.$broadcast('show-errors-check-validity');
      
      if ($scope.newEquipmentForm.$valid) {
        var equipment = new Equipment($scope.equipment);
        equipment.$save(equipment, 
          function(equipmentResponse) {
            $state.go('app.equipments.detail', {equipmentId: equipmentResponse.id});
          },
          function(errorResponse) {
            $scope.errors['form'] = errorResponse.error;
            $scope.newEquipmentForm['location'].$setValidity('server', false);
            $scope.newEquipmentForm['model'].$setValidity('server', false);
            $scope.newEquipmentForm['equipment_number'].$setValidity('server', false);
            $scope.newEquipmentForm['serial_number'].$setValidity('server', false);
            $scope.newEquipmentForm['make'].$setValidity('server', false);
          }
        );
      }
    };
  }]);