'use strict';

var equipmentDetailController = angular.module('equipmentDetailController', []);

equipmentDetailController.controller('EquipmentDetailCtrl', ['$scope', '$state', '$stateParams', '$alert', '$modal', 'Equipment',
  function($scope, $state, $stateParams, $alert, $modal, Equipment) {
    $scope.equipment = Equipment.get({id: $stateParams.equipmentId}, function() {
      if ($scope.$parent.currentEquipment == null) {
        $scope.$parent.currentEquipment = $scope.equipment;
      }
    });

    $scope.activeForm = "colorimeters";

    $scope.createModalContentTemplate = {
      colorimeters: "colorimeter/new/colorimeter-new.html",
      ph_calibrations: "phCalibration/new/ph-calibration-new.html"
    };

    var createModal = $modal({template:$scope.createModalContentTemplate[$scope.activeForm], show: false});

    $scope.setActiveForm = function(form) {
      $scope.activeForm = form;
      createModal = $modal({template:$scope.createModalContentTemplate[$scope.activeForm], show: false});
    }

    $scope.openCreateFormModal = function() {
      createModal.$promise.then(createModal.show);
    }

    $scope.$on("new-form-success", function() {
      createModal.$promise.then(createModal.hide);
    });

    $scope.delete = function() {
      var equipment = new Equipment($scope.equipment);
      delete equipment.colorimeters;
      delete equipment.ph_calibrations;
      equipment.$delete(equipment, function(equipmentResponse) {
        var equipmentDeletedAlert = $alert({
          title: 'Equipment deleted!', 
          content: 'You have successfully deleted this equipment.',
          type: 'success'
        });

        $state.go('app.equipments', {}, {reload: true});
      });
    }

    $scope.activeFormToViewString = function(form) {
      switch(form) {
        case "colorimeters":
          return "Colorimeter";
        case "ph_calibrations":
          return "pH Calibration";
        case "chlorine_meters":
          return "Chlorine Meter";
        case "hach_turbiditys":
          return "Hach Turbidity Meter";
        default:
          break;
      }
    }
  }
]);