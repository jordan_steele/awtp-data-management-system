'use strict';

var userListController = angular.module('userListController', []);

userListController.controller('UserListCtrl', ['$scope', '$location', 'User', 
  function ($scope, $location, User) {
    $scope.users = User.query();
    var currentUserParentSetByClick = false;

    if ($scope.$parent.currentUserParent == null) {
      var currentUserParentListener = $scope.$watch (
        function() {
          return $scope.$parent.currentUserParent;
        },
        function(newValue, oldValue) {
          if (newValue != oldValue) {
            if (!currentUserParentSetByClick) {
              $scope.query = $scope.$parent.currentUserParent.email;
            }
            currentUserParentListener();
          }
        })
    }

    $scope.updateCurrentUserParent = function(user) {
      $scope.$parent.currentUserParent = user;
      currentUserParentSetByClick = true;
    }
  }]);