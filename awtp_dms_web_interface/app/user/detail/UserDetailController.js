'use strict';

var userDetailController = angular.module('userDetailController', []);

userDetailController.controller('UserDetailCtrl', ['$scope', '$state', '$stateParams', '$alert', 'User',
  function($scope, $state, $stateParams, $alert, User) {
    $scope.user = User.get({id: $stateParams.userId}, function() {
      if ($scope.$parent.currentUserParent == null) {
        $scope.$parent.currentUserParent = $scope.user;
      }
      $scope.adminSwitch = $scope.user.admin;
    });

    $scope.update = function() {
      var user = new User($scope.user);
      user.admin = $scope.adminSwitch;
      delete user.created_at
      user.$update(user, function(userResponse) {
        var userUpdatedAlert = $alert({
          title: 'User updated!', 
          content: 'You have successfully updated this user.',
          type: 'success'
        });
      });
    }

    $scope.delete = function() {
      var user = new User($scope.user);
      user.$delete(user, function(userResponse) {
        var userDeletedAlert = $alert({
          title: 'User deleted!', 
          content: 'You have successfully deleted this user.',
          type: 'success'
        });

        $state.go('app.users', {}, {reload: true});
      });
    }
  }
]);